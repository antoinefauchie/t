// this script wil put any element with the property
// `position: fill page` on the next page while keeping the rest of the content flowing.
// could be look at page-float: next top;
//
//
//a fair warning, if the element is longer than a page, we need to dig it a little bit, as there is a designer question.
// /we shouldn’t answer in the place of the designer. 
//
//
// use:
//
// ---===---===---
//
//   .elementToFill {
//     position: fill-page;
//   }
//
// ---===---===---
//
// the pagedjs-fillpage template is created by the script to manage the layout of the fullpage layout
//
//this will try to fill up the page with any image coming up from the content
//
//
//

// how it works:
// when there is a node with the right class list to the page,
// check the remaining space. If there isn’t enough, move the elment to an arrya
//
// when you create the page, check if there is something in the array.
// if so start the page with the element (or addit to the wrapper)
//
// if there is enough space, leave it be.

const fillPageClass = "pagedjs-fill-next-page";

class fillpage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.fillPageElements = [];
    this.pushToNextPage = [];
    this.putThatFirst = [];
    this.needPage = [];
  }

  //read the css to find the elements that will automatically fill page
  onDeclaration(declaration, dItem, dList, rule) {
    if (declaration.property == "position") {
      if (declaration.value.children.head.data.name.includes("fill-page")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        // if it happened after the other script, just remove the data id from there.
        sel = sel.replace('[data-id="\\"', "#");
        sel = sel.replace('\\""]', "");
        this.fillPageElements.push(sel.split(","));
      }
    }
  }

  async afterParsed(parsed) {
    // add the width and height to the image as attribute to get more data about what they are
    await addWidthHeightToImg(parsed);

    //find from the css the element you wanna have  full page
    if (("this", this.fillPageElements)) {
      this.fillPageElements.forEach((selector) => {
        parsed.querySelectorAll(selector).forEach((el) => {
          el.classList.add(fillPageClass);
        });
      });
    }
  }

  async layoutNode(node) {
    // find a way to leave it if the node has enough room on the page
    //if node not element
    if (node.nodeType != 1) return;

    // if node has the page fill class
    if (node.classList.contains(fillPageClass)) {
      // check the remaining space & check the element height
      let height = await getHeightOfHiddenElement(node);
      let spaceOnPage = await getRemainingSpaceOnPage(getLastPage());

      let pageHeight = getLastPage().querySelector(
        ".pagedjs_page_content",
      ).offsetHeight;

      console.log(height);
      console.log(pageHeight);
      console.log(spaceOnPage);
      console.log(node)

      if (height > pageHeight * 0.8) {

        // create a page just for the element
        // create a style for it on a custom page
      } else if (height > spaceOnPage) {
        // hide the element
        node.style.display = "none";


        // would be amazing, but impossible
        // if (nodeTemplate.nextElementSibling) {
        //   nodeTemplate.nextElementSibling.insertAdjacentElement(
        //     "afterend",
        //     nodeTemplate,
        //   );
        // }

        // push the element to the node
        let newnode = node.cloneNode(true);
        newnode.id += "clone";
        newnode.classList.add("ok-to-fill");
        newnode.style.display = "block";
        this.putThatFirst.push(newnode);
      } else {
        // do nothing, the element has enough room
      }
    }
  }

  async finalizePage(page) {
    // if there is a hidden block element with a ok-to-fill and fillpageclass, check its height, and if it’s more than 80% of the previous page, make it on its own page
    // console.log(page);
  }

  async onPageLayout(page, Token, layout) {
    while (this.putThatFirst.length > 1) {
      // get the first element while removing it from the array
      const elem = this.putThatFirst.shift();

      // check if there is enough room to put the element, and make sure that it’s not a continuted figure,

      if (page.hasChildNodes()) {
        console.log(await getHeightOfHiddenElement(elem));
        console.log(await getRemainingSpaceOnPage(page));
        // check the room on the page
        if (
          (await getHeightOfHiddenElement(elem)) <
          (await getRemainingSpaceOnPage(page))
        ) {
          // check if there is enough room, otherwise add a page
          elem.className = "pagedjs-filler-original";
          elem.style.display = "block";
          // elem.style.marginBottom = "17px";
          page.insertAdjacentElement("afterbegin", elem);
        } else {
          this.needPage.push(elem);
        }
      } else {
        //nochild
        console.log("no child");
        elem.className = "pagedjs-filler-original";
        elem.style.display = "block";
        // elem.style.marginBottom = "17px";
        page.insertAdjacentElement("afterbegin", elem);
      }
    }
  }
}

Paged.registerHandlers(fillpage);

async function addWidthHeightToImg(content) {
  let imagePromises = [];
  let images = content.querySelectorAll("img");
  images.forEach((image) => {
    let img = new Image();
    let resolve, reject;
    let imageLoaded = new Promise(function(r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function() {
      let height = img.naturalHeight;
      let width = img.naturalWidth;
      image.setAttribute("height", height);
      image.setAttribute("width", width);
      resolve();
    };
    img.onerror = function() {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  return Promise.all(imagePromises).catch((err) => {
    console.warn("err", err);
  });
}

// get the homothetic reduce height when reducing the width to see if the image can get in.
// with a baseline
function getHeight(originalWidth, originalHeight, reducedWidth, baseline) {
  const homotheticHeight = (reducedWidth * originalHeight) / originalWidth;
  const baselineHeight = Math.floor(homotheticHeight / baseline) * baseline;
  return Math.floor(baselineHeight);
}

function checkImageHeightRatio(imageHeight, availableSpace) {
  return imageHeight / availableSpace;
}

function resizeToBaseline(originalWidth, originalHeight, baselineGrid) {
  // Calculate the new height relative to the baseline grid
  const baselineHeight =
    Math.floor(originalHeight / baselineGrid) * baselineGrid;

  // Calculate the width reduction ratio based on the new height
  const reductionRatio = baselineHeight / originalHeight;

  // Calculate the new width proportionally
  const reducedWidth = originalWidth * reductionRatio;

  // Return the reduced dimensions as an object
  return {
    width: reducedWidth,
    height: baselineHeight,
  };
}

//check if the element is the the nessted first child of the page
function isNestedFirstChild(childElement, parentElement) {
  const firstChild = parentElement.firstElementChild;
  return firstChild === childElement;
}

// check if element is empty
function isElementEmpty(element) {
  return element.textContent.trim() === "";
}

// function to move thing to a new page
//
//
//

function getLastBlockOffsetBottom(element) {
  // console.log(element.children)
  // if (!element.hasChildNodes) return 0;
  const children = element.children;
  let lastBlockOffsetBottom = 0;

  for (let i = children.length - 1; i >= 0; i--) {
    const child = children[i];

    // Check if the child is a block-level element (by checking it’s display)
    const display = window.getComputedStyle(child).display;
    // Calculate the cumulative offset bottom
    const childOffsetBottom = child.offsetTop + child.offsetHeight;

    // Update the last block offset bottom if it's greater
    if (childOffsetBottom > lastBlockOffsetBottom) {
      lastBlockOffsetBottom = childOffsetBottom;
    }

    // Recursively check nested elements
    const nestedBlockOffsetBottom = getLastBlockOffsetBottom(child);
    if (nestedBlockOffsetBottom > lastBlockOffsetBottom) {
      // console.log(nestedBlockOffsetBottom);
      // console.log(lastBlockOffsetBottom);
      lastBlockOffsetBottom = nestedBlockOffsetBottom;
    }
  }

  // return the offset bottom in px (number)
  return lastBlockOffsetBottom;
}
async function getRemainingSpaceOnPage(page, security = 40) {
  const parentHeight = parseInt(
    window.getComputedStyle(
      getLastPage().querySelector(".pagedjs_page_content"),
    ).height,
  );
  console.log(parentHeight);
  console.log(getLastPage());
  let result = parentHeight - getLastBlockOffsetBottom(page) - security;
  // console.log("result", result);
  return result;
}

function getLastPage() {
  // create a page variable which is the one pagedjs is working on.
  // use latest page to get the latest page
  let pages = document.querySelectorAll(".pagedjs_page");
  const latestPage = pages[pages.length - 1];
  return latestPage;
}

// WIDTH
// current image width / biggest img * 100%;

// this let us find a correspondances of width for the image we get.
// really experimental

async function getHeightOfHiddenElement(element) {
  // find a page to render on
  let pageToRenderOn = getLastPage();

  let clone = element.cloneNode(true);
  clone.style.position = "absolute";
  clone.style.top = "0";
  clone.style.left = "0";
  clone.style.display = "block";

  // insert a clone of that element
  pageToRenderOn
    .querySelector(".pagedjs_page_content")
    .insertAdjacentElement("afterbegin", clone);

  // get margin in case there are some (margins also come with trouble)
  let margins = clone.style.marginTop + clone.style.marginBottom;

  // get the height of the elements + the margins
  let elementHeight = clone.offsetHeight + margins;

  // delete the clone we used to get the height
  clone.remove();

  // return the height as number
  return elementHeight;
}
