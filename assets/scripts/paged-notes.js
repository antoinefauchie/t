const lookingFor = ".renvoi, .note, .hugo-cite-citation, .legende";

class pagedjsNotes extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  beforeParsed(content) {
    content.querySelectorAll(lookingFor).forEach((el) => {
      el.classList.add("chaussette");
    });

    // reset number for note in the margins
    //add note number with real number instead of counters to use it in the margins
    content.querySelectorAll(".chapitre").forEach((chapter) => {
      chapter.querySelectorAll(".note-numero").forEach((note, index) => {
        if (note.nextElementSibling) {
          note.dataset.noteCounter = index + 1;
          note.nextElementSibling.dataset.noteCounter = index + 1;
        }
      });
    });
    content.querySelectorAll(".regulier").forEach((chapter) => {
      chapter.querySelectorAll(".note-numero").forEach((note, index) => {
        if (note.nextElementSibling) {
          note.dataset.noteCounter = index + 1;
          note.nextElementSibling.dataset.noteCounter = index + 1;
        }
      });
    });
  }
  async finalizePage(page) {
    const blockThingy = document.createElement("div");
    blockThingy.classList.add("renvoiBlock");

    let pageElements = page.querySelectorAll(lookingFor);

    pageElements.forEach((el, index) => {
      // pour mettre les légende des images en top
      if (el.classList.contains("legende")) {
        let topAnchor;
        if (el.closest("figure").querySelector("img")) {
          topAnchor = el.closest("figure").querySelector("img");
        } else if (el.closest("figure").querySelector("pre")) {
          topAnchor = el.closest("figure").querySelector("pre");
        }
        el.dataset.offsetTop = topAnchor?.offsetTop >= 0
          ? topAnchor.offsetTop
          : el.offsetTop;
      } else if (el.classList.contains(".hugo-cite-citation")) {
        const topAnchor = el
          .closest("figure")
          .querySelector(".hugo-cite-intext");
        el.dataset.offsetTop = topAnchor?.offsetTop
          ? topAnchor.offsetTop
          : el.offsetTop;
        if (el.previousElementSibling) {
          el.previousElementSibling.dataset.offsetTop =
            el.previousElementSibling?.offsetTop;
        }
      } else {
        if (el.previousElementSibling) {
          el.previousElementSibling.dataset.offsetTop =
            el.previousElementSibling?.offsetTop;
        }
        el.dataset.offsetTop = el.offsetTop;
      }
      let offset = 0;
      el.style.top = el.dataset.offsetTop - offset + "px";
      el.style.position = "absolute";
      blockThingy.insertAdjacentElement("beforeend", el);
    });

    page
      .querySelector(".pagedjs_page_content")
      .insertAdjacentElement("beforeend", blockThingy);

    // try to move things here
    //  once the page is done, check the whole thing

    const allnotes = page.querySelectorAll(".chaussette");

    //height of all the notes
    const pageHeight = page.querySelector(".pagedjs_page_content").offsetHeight;
    let noteHeight = 0;

    allnotes.forEach((note) => {
      noteHeight += note.offsetHeight;
    });

    // add check to see when there is more note than page height so the author can make a decision.
    if (noteHeight > pageHeight) {
      allnotes.forEach((note) => {
        note.style.position = "unset";
        note.style.marginBottom = ".3em";
        note.style.color = "green";
      });
    }

    // move notes bottom when they touch each others.
    // add some text indent to the note? or reverse text indent?
    allnotes.forEach((el) => {
      let previousNote = el.previousElementSibling;
      if (previousNote) {
        if (
          previousNote?.offsetTop + previousNote?.offsetHeight >=
          el.offsetTop
        ) {
          // put them in the same div and manage the couples of notes with the div.
          el.classList.add("overlap");
          el.dataset.topLocation =
            previousNote.offsetTop + previousNote.offsetHeight;
          el.style.top = el.dataset.topLocation + "px";
          // console.log(page.querySelector(".pagedjs_page_content").offsetHeight)
        }
      }
    });

    //check if the notes goes too down
    //

    // if there is not note get back
    if (allnotes.length == 0) return;

    let lastnote = [...allnotes].pop();

    // check if page overflow
    let pageOverflow =
      Number(lastnote.offsetHeight) + Number(lastnote.dataset.offsetTop);

    if (pageOverflow > pageHeight) {

      //put the last note at the bottom and check the height to push up anything touching.
      lastnote.style.top = "unset";
      lastnote.style.bottom = 0;
      //while last note overlap the previous note
      while (
        lastnote.offsetTop <
        lastnote.previousElementSibling?.offsetHeight +
        lastnote.previousElementSibling?.offsetTop
      ) {
        lastnote.previousElementSibling.style.top = "unset";
        lastnote.previousElementSibling.style.bottom = lastnote.offsetHeight + 8 + "px";
        lastnote = lastnote.previousElementSibling;
      }
    }
    //
    // let pageOverflow =
    // console.log("overflow: ", pageOverflow);
    // if (pageOverflow) {
    //   document.querySelectorAll(".overlap").forEach((overlap) => {
    //     overlap.dataset.offsetTop -= pageOverflow;
    //   });
    // }

    // if there is a hidden block element with a ok-to-fill and fillpageclass, check its height, and if it’s more than 80% of the previous page, make it on its own page
    // debugger
    //
    //
  }

  afterRendered(pages) { }
}
Paged.registerHandlers(pagedjsNotes);

/* No hyphens between pages */
/* warning : may cause polyfill errors */


class noHyphenBetweenPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.hyphenToken;
  }

  afterPageLayout(pageFragment, page, breakToken) {

    if (pageFragment.querySelector('.pagedjs_hyphen')) {

      // find the hyphenated word
      let block = pageFragment.querySelector('.pagedjs_hyphen');

      // i dont know what that line was for :thinking: i removed it
      // block.dataset.ref = this.prevHyphen;

      // move the breakToken
      let offsetMove = getFinalWord(block.innerHTML).length;

      // move the token accordingly
      page.breakToken = page.endToken.offset - offsetMove;

      // remove the last word
      block.innerHTML = block.innerHTML.replace(getFinalWord(block.innerHTML), "");

      breakToken.offset = page.endToken.offset - offsetMove;

    }
  }

}

Paged.registerHandlers(noHyphenBetweenPage);

function getFinalWord(words) {
  var n = words.split(" ");
  return n[n.length - 1];
}
