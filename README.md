# Dépôt de la thèse d'Antoine Fauchié

Ce dépôt regroupe les sources de la thèse d'[Antoine Fauchié](https://www.quaternum.net), doctorant à l'Université de Montréal (Canada).


## Informations institutionnelles

Cette thèse est dirigée par Marcello Vitali-Rosati et codirigée par Michael Sinatra.

Titre : _Fabriquer des éditions, éditer des fabriques : reconfiguration des processus techniques éditoriaux et nouveaux modèles épistémologiques_


## Organisation du dépôt

Les fichiers suivent l'organisation imposée par le générateur de site statique Hugo :

- les _contenus_ sont placés dans le répertoire `content`, et les chapitres de la thèse sont plus particulièrement dans le dossier `p` ;
- les _gabarits_ sont dans le répertoire `layouts`, avec des distinctions concernant ceux dédiés à la version web et ceux pour la version paginée ;
- la configuration principale est décrite dans le fichier `config.toml`, avec des environnements spécifiques dans le répertoire `config`.


## Prévisualiser et déployer

[Hugo v0.111.3](https://github.com/gohugoio/hugo/releases/tag/v0.111.3) est nécessaire pour pouvoir prévisualiser et déployer la thèse :

- `hugo server --disableFastRender` pour _servir_ le site en local ;
- `hugo --cleanDestinationDir` pour déployer/générer les fichiers de la thèse ;
- `hugo server --disableFastRender --environment impression` pour prévisualiser la version paginée de la thèse, _servie_ en local à l'adresse `http://localhost:1313/compilation/impression.html`.


## Thèse en production

La thèse est disponible en ligne, pour sa version de production, à cette adresse : [https://these.quaternum.net](https://these.quaternum.net/)


## Licence(s)

Les textes et les gabarits sont placés sous licence [Creative Commons CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
Certains programmes ou composants ont une licence différente : Hugo est sous licence Apache-2.0, Paged.js est sous licence MIT, et la police typographique [Fern de David Jonathan Ross](https://djr.com/fern) est sous licence _educationnal_.


## Signalement d'une erreur

Pour signaler une erreur, trois possibilités :

- écrire à [Antoine](mailto:antoine@quaternum.net) ;
- lui envoyer un _patch_ (aide : [https://git-send-email.io](https://git-send-email.io)) ;
- interagir avec le dépôt miroir : [https://gitlab.huma-num.fr/antoinefauchie/t](https://gitlab.huma-num.fr/antoinefauchie/t).

