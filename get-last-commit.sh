#!/usr/bin/env bash

export HUGOxPARAMSxGITxLAST_COMMITxAUTHORNAME=$(git log -1 --format=%an)
export HUGOxPARAMSxGITxLAST_COMMITxDATE=$(git log -1 --format=%as)
export HUGOxPARAMSxGITxLAST_COMMITxHASH=$(git log -1 --format=%h)
export HUGOxPARAMSxGITxLAST_COMMITxSUBJECT=$(git log -1 --format=%s)
export HUGOxPARAMSxGITxVERSIONxNUMBER=$(git describe --abbrev=0 --tags)
export HUGOxPARAMSxGITxVERSIONxDESCRIPTION=$(git tag -l --format='%(subject)' | tail -1)
