#!/bin/bash

pandoc -f markdown -t html --citeproc --toc --template=layouts/template-p.html content/parties/partie-01.md -o public/partie-01.html

pandoc -f markdown -t html --citeproc --toc --template=layouts/template-p.html content/parties/partie-02.md -o public/partie-02.html

cp -r static/* public/static/.

rsync -avz --delete public/ quaternu-t@ftp.cluster003.hosting.ovh.net:
