
# mettre en cache les informations liées à Git (que Hugo n'est pas capable de gérer)
git = bash get-last-commit.sh

# commande pour servir le site en local
serve = hugo server --disableFastRender

# commande pour générer les fichiers HTML
gen = hugo --cleanDestinationDir

# modification du PDF pour inclure des métadonnées
pdf_metadata = exiftool -Author="Antoine Fauchié" -Title="Fabriquer des éditions, éditer des fabriques : reconfiguration des processus techniques éditoriaux et nouveaux modèles épistémologiques" -Subject="édition, édition numérique, chaîne d'édition, éditorialisation, fabrique d'édition, littérature" static/documents/antoine-fauchie-these-v1-1.pdf

# commande pour synchroniser les fichiers
rsync = rsync -avz --delete public/

# environnement de développement
env = quaternu-t@ftp.cluster003.hosting.ovh.net:

# environnement de production
prod = quaternu-these@ftp.cluster003.hosting.ovh.net:

# combinaison commune pour une génération complète
gen_comp = $(git) && $(gen) && $(pdf_metadata) && $(rsync)

# servir en local la version web sans la version paginée
s:
	$(git) && $(serve) -p 1717

# servir en local la version web et la version paginée
si:
	$(git) && $(serve) --environment impression -p 1717

# déploiement en développement
depl_dev:
	$(gen_comp) $(env)

# déploiement en production
depl_prod:
	$(gen_comp) $(prod)

# déploiement en production sans la re-génération des métadonnées du PDF
depl_prod_simple:
	$(git) && $(gen) && $(rsync) $(prod)

