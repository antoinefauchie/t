---
title: "Avant-propos"
weight: 80
table_des_matieres: "avant"
contenu: "static"
visibilite: "web"
---

### Un travail de recherche performatif

Cette thèse est une démonstration performative, certaines des méthodes et des techniques issues des expérimentations présentées dans les études de cas sont reprises pour les modes d'écriture et d'édition.
Les objets web et paginés sont le résultat d'un travail de balisage du texte, de modélisation via des gabarits pour les versions web et paginée, ainsi que de l'élaboration de certains scripts pour l'affichage contextuel de certaines informations (telles que les renvois ou les définitions des concepts par exemple) ou pour l'automatisation de la génération des formats de sortie.
Cela explique l'effort mis dans la valorisation de ce travail, via un accès aux sources versionnées, qui comprennent les textes et les gabarits.
Cet accès est permis tout au long de la thèse via des indications en regard de chaque chapitre et section — avec des liens hypertextes pour la version web.


### Artefacts de la thèse et lectures

Cette thèse est disponible dans deux formats via l'adresse [https://these.quaternum.net](these.quaternum.net) : un format _web_ constitué d'un ensemble de pages HTML, et un format PDF aussi appelé "version paginée".

{{< apparitionp >}}
Cette version paginée, imprimable ou imprimée, ne propose pas toutes les fonctionnalités de la version web, et notamment l'accès aux sources via des URLs.
Nous renvoyons à la version web pour disposer d'un environnement de lecture qui a été pensé en même temps que l'écriture de la thèse.
{{< /apparitionp >}}

{{< apparitionw >}}
Cette version web a été réalisée en même temps que l'écriture de la thèse, elle invite à des renvois entre les chapitres et les sections, ainsi qu'aux différentes définitions des concepts invoqués et développés.
{{< /apparitionw >}}


### Montréal, Tiohtá:ke, Mooniyaang 

Cette recherche a été menée à Montréal et plus spécifiquement à l'Université de Montréal, celle-ci reconnaît qu'elle est située en territoire autochtone non cédé par voie de traité.
L'Université de Montréal salue ceux et celles qui, depuis des temps immémoriaux, en ont été les gardiens traditionnels, et exprime son respect pour la contribution des peuples autochtones à la culture des sociétés ici et partout autour du monde.

