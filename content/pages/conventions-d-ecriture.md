---
title: "Conventions d'écriture"
weight: 70
table_des_matieres: "avant"
contenu: "static"
visibilite: "web"
---

### Écritures inclusives

La rédaction de cette thèse suit les principales recommandations de l'Université de Montréal en matière d'écriture inclusive{{< n >}}[https://francais.umontreal.ca/ressources-et-formations/inclusivement/presentation/](https://francais.umontreal.ca/ressources-et-formations/inclusivement/presentation/){{< /n >}}, notamment en privilégiant les formes épicènes ou le _doublet complet_.
Nous appliquons également l'accord de proximité, qui consiste à accorder en genre l'adjectif et le participe passé avec le nom le plus proche qu'il qualifie.
En plus de mettre en pratique des méthodes d'écriture inclusive, nous avons également tenté d'appliquer une écriture non sexiste, par exemple en rendant visible le genre féminin souvent invisibilisé.


{{< apparitionp >}}
### Mise en page de la version imprimée

L'usage d'une colonne pour faire apparaître les notes, les renvois, les références bibliographiques ou les définitions des concepts est une adaptation d'un principe de composition hérité — notamment — d'Edward Tufte.
Ce principe est plus récemment adopté et revendiqué pour la mise en page de travaux de recherche.
Parmi plusieurs initiatives, celles d'Arthur Perret sont exemplaires et ont beaucoup inspiré certains choix de mise en page.
La composition du texte de labeur, ferré à gauche et en drapeau, est un choix volontaire qui vise à faciliter la lecture avec un espacement constant entre les mots.
{{< /apparitionp >}}


### Affichage des références et style bibliographique

Les références bibliographiques sont systématiquement indiquées dans la marge, en regard de la citation dans le texte, pour permettre une visibilité immédiate des références sans avoir à naviguer dans le document (web ou paginé).
Toutes les références apparaissent par ailleurs dans les bibliographies (une seule pour la version paginée).
Le style bibliographique utilisé est le style APA (version française développé notamment par les Bibliothèques de l'Université de Montréal) avec quelques ajustements.


### Indication des commits et accès aux sources

En regard de chaque unité textuelle du corps de la thèse — texte introductif de chaque chapitre, et sections — sont affichées plusieurs informations : l'identifiant du dernier commit sur la version paginée, auquel s'ajoute le lien vers la source du document sur la version web.
Ainsi, une mention telle que `<bbe05b4>` apparaît en regard des titres de chapitre et des titres de section (également pour l'introduction et la conclusion), permettant d'identifier les dernières modifications, voire l'historique complet du fichier source concerné.


### Remarques sur les citations dans leur langue originale

Suivant les recommandations du Département de littératures et de langues du monde de l'Université de Montréal, les citations longues sont laissées dans leurs langues originales, en l'occurrence l'anglais quand il ne s'agit pas du français.
Le contexte de la thèse justifie également ce choix, puisque les recherches se sont déroulées majoritairement au Canada avec des communautés francophones et anglophones.

