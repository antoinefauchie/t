---
title: "Colophon"
weight: 120
table_des_matieres: "apres"
visibilite: "web"
---

Cette thèse a été écrite avec l'éditeur de texte Vim, dans un terminal Gnome, sur des ordinateurs portables ThinkPad X13 sous Ubuntu et ThinkPad T480 sous Debian.
Les fichiers sont versionnés avec Git, les dépôts distants sont hébergés par SourceHut, Codeberg, Huma-Num et Ionos.
Le générateur de site statique Hugo a permis de modéliser les textes et de _fabriquer_ les artefacts (formats web et paginé).
Le convertisseur Pandoc a été utilisé pendant les premières phases de rédaction, ainsi que LaTeX et la distribution TeXlive.
La version paginée a été produite avec Paged.js (et l'aide précieuse de Julien Taquet pour la gestion des marges) et générée avec Chromium.
La version web est hébergée par Ionos et OVH.

La police typographique, Fern, est une création de David Jonathan Ross, sous licence _educational_.

