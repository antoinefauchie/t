---
title: "Acronymes et sigles"
weight: 55
table_des_matieres: "avant"
contenu: "static"
visibilite: "web"
---

### API
Une API, ou _Application Programming Interface_, est une interface de programmation d'application qui permet d'accéder à des informations structurées via des méthodes décrites.

### AST
L'AST, ou _Abstract Syntaxic Tree_, est un arbre syntaxique abstrait qui constitue une modélisation abstraite d'un document structuré.

### CMS
Un CMS, ou _Content Management System_, est un système de gestion de contenus, habituellement proposé sous la forme d'un logiciel avec une interface graphique.

### Coko
Coko, pour Collaborative Knowledge Foundation{{< n >}}[https://coko.foundation](https://coko.foundation){{< /n >}}, est une fondation à but non lucratif qui crée, développe et maintien des outils libres, notamment en lien avec l'édition ou la publication.

### DOM
Le DOM, ou _Document Object Model_, est un modèle d'objets de document, il s'agit de la manifestation abstraite d'un document dont les composants sont manipulables.

### GML
GML, pour _Generalized Markup Language_, est le prédécesseur du premier langage de balisage _descriptif_, SGML (voir SGML), et est plus spécifiquement un point d'articulation entre un balisage _procédural_ et un balisage _descriptif_.

### HTML
HTML, pour _HyperText Markup Language_, est un balisage standard et descriptif conçu pour la structuration sémantique et l'affichage graphique dans les navigateurs web.

### IDE
Un IDE est un environnement de développement, _Integrated Development Environment_ en anglais, qui regroupe un ensemble d'outils tels qu'un éditeur de texte, un terminal ou des fonctions de _versionnement_ et de déploiement.

### OSP
Open Source Publishing{{< n >}}[http://osp.kitchen](http://osp.kitchen){{< /n >}} est un collectif de designers dont ses membres sont basés en Europe et principalement en Belgique.

### PAO
La publication assistée par ordinateur correspond à l'usage de logiciels pour la structuration et la composition de documents qui sont ensuite imprimés.
Traditionnellement, la PAO doit permettre de gérer des contenus sous forme de pages.

### RFC
Les _Requests for Comments_ sont des spécifications techniques qui reposent sur un système de soumission et de validation{{< n >}}[https://www.rfc-editor.org](https://www.rfc-editor.org){{< /n >}}, ces spécifications constituent notamment le socle technique d'Internet.

### SaaS
_Software as a Service_, ou logiciel en tant que service en français, définit des logiciels qui sont hébergés sur des serveurs et dont l'usage se fait via une connexion Internet (et souvent un navigateur web) sans avoir besoin d'installer le logiciel.

### SGML
_Standard Generalized Markup Language_ est le premier langage de balisage _descriptif_ qui permet de décrire sémantiquement un document et de séparer strictement sa structure de sa mise en forme.
SGML hérite des recherches autour de GML (voir GML) et connaît de nombreuses implémentations dont HTML (voir HTML) constitue la plus répandue.

### WYSIWYG
_What You See Is What You Get_, ou ce que vous voyez est ce que vous obtenez, correspond à un mode d'écriture et d'édition basé sur la représentation graphique plutôt que sur le sens.

### WYSIWYM
_What You See Is What You Mean_, ou ce que vous voyez est ce que vous signifiez, est un mode d'écriture et d'édition qui s'oppose au WYSIWYG (voir WYSIWYG).

### XML
XML, pour _Extensible Markup Language_, est un métalangage de balisage descriptif qui peut être décliné pour des usages très divers grâce à l'élaboration d'un schéma.

