---
title: "Remerciements"
weight: 60
table_des_matieres: "avant"
contenu: "static"
draft: false
visibilite: "web"
---

Mes remerciements vont d'abord à mes directeur et codirecteur de recherche, pour leur soutien inconditionnel durant tout le doctorat.
Merci à Marcello Vitali-Rosati, pour cette opportunité de _faire_ de la recherche, pour la possibilité de construire ensemble et chaque jour de nouvelles modélisations.
Ce cheminement a été possible grâce à sa bienveillance et à sa rigueur, toujours avec une énergie incroyable.
Merci à Michael Sinatra, pour m'avoir connecté à des communautés académiques accueillantes, et pour m'avoir enseigné certains des mécanismes complexes de l'Université.
Son accompagnement sans failles a été décisif dans la réalisation de ce projet universitaire et professionnel.

Merci à Evelyne Broudoux et à Emmanuël Souchier pour avoir évalué mon travail.

Cette thèse doit beaucoup à Margot Mellet, chercheuse talentueuse, éditrice acharnée, première relectrice, et amie.
Nous écrivons, paraît-il, d'abord pour une personne, cette thèse a été rédigée en me demandant continuellement ce qu'en penserait Margot.

Les projets présentés dans cette thèse sont initiés, menés, développés, _fixés_, parfois abandonnés, souvent défendus, par des personnes curieuses et enrichissantes.
Merci à David Larlet, Guillaume Grossetie, Thomas Parisot, Timothée Guicherd, Maïtané Lenoir, et Yves Marcoux.
Je remercie plus particulièrement Hélène Beauchef, Jean-François Vallée, Nicolas Sauret, Servanne Monjour, chacune et chacun pour des raisons différentes, mais toutes et tous pour être une source d'inspiration et de motivation.

Merci à Roch Delannay pour avoir nourri mes réflexions (et parfois aussi mon estomac), infatigable répondant à mes interrogations, et insatiable interlocuteur sur de nombreuses thématiques qui sont présentes dans ce texte.

La Chaire de recherche du Canada sur les écritures numériques est une _fabrique_ de pensée à laquelle ont contribué, ou contribuent encore, beaucoup de petites mains (dont je suis) dont la richesse intellectuelle et humaine est inestimable.
Merci à Eugénie Matthey-Jonais, Giulia Ferretti, Emmanuelle Lescouet, François Maltais-Tremblay, Charlotte Lebon, Yann Audin, Arilys Jia, Luiz Capelo, Enrico Agostini Marchese.
Un merci spécial à Mathilde Verstraete, vraiment, ainsi qu'à Louis-Olivier Brassard.

Merci à Julie Blanc, dont les collaborations théoriques et pratiques  m'ont beaucoup apporté dans la constitution de ce travail de recherche.

Merci à Rekka Bellum et Devine Lu Linvega, qui, au-delà de leur générosité, offrent des perspectives aussi radicales qu'enthousiasmantes.

Merci à Julien Taquet qui, en plus de m'avoir permis de résoudre nombre de problèmes liés à la composition de la version paginée de la thèse, est un fin connaisseur des rouages numériques et imprimés.

De près ou de loin, et sur des plans particulièrement variés, plusieurs personnes ont été d'un soutien considérable dans cette thèse.
Je remercie vivement Julien Bidoret, Constance Crompton, Igor Milhit, Benoît Epron, Valérie Larroche, Susan Brown, Benoît Melançon, Nicolas Taffin, Enzo Poggio, John Maxwell, Emmanuel Château-Dutier, Joe Mooring.

Merci à Gabrielle Pannetier Leboeuf pour les tomates dans l'insectarium.
L'écriture est une question de rythme, et celui de Gabrielle m'a clairement permis de trouver ma mesure.

Merci aux personnels administratifs de l'Université de Montréal, et en premier lieu à Kathy Leduc.

Merci aux bibliothécaires et aux aides bibliothécaires de la bibliothèque Lettres et sciences humaines de l'Université de Montréal, ce travail doit aussi beaucoup aux ressources documentaires et humaines toujours disponibles.

Merci aux collectifs Inachevé d'imprimer (et d'abord à Arthur Perret) et PrePostPrint qui animent et agitent.

Merci à celles et à ceux qui me soutiennent ici et ailleurs : Elyane, Michel, Marie, Jean, Laetitia, Bernard, Marie-Thérèse, Yannick, Julien, Thomas.
Une mention spéciale pour Michel, pour sa disponibilité, et son œil curieux et avisé.

Merci à Vimala pour les encouragements, pour les joies, pour le temps perdu et la vie gagnée, et surtout pour m'avoir extrait trop souvent de la thèse.

Ce travail de recherche s'est déroulé dans des conditions intellectuelles et affectives qui débordent largement le cadre du doctorat, et dont celui-ci s'est nourri abondamment — parfois malgré moi.
La _thèse_ est un exercice d'exploration, d'analyse, de réflexion critique et d'écriture qui ne peut être réalisé qu'avec des apports culturels riches, des visions du monde politiques et positives, et une radicalité esthétique permanente.
Je dois beaucoup de tout cela à Isabelle, qui a su me montrer que toute existence ne peut se réaliser qu'avec le désir de vivre pleinement, tout en brisant les codes qui nous oppressent — y compris ceux du doctorat.
Merci.

---

Cette recherche a été soutenue par le Fonds de recherche du Québec - Société et culture, ainsi que par la Chaire de recherche du Canada sur les écritures numériques, le Groupe de recherche sur les éditions critiques en contexte numérique, le Centre de recherche interuniversitaire sur les humanités numériques, le Département de littératures et de langues du monde de l'Université de Montréal, et le Département des littératures de langue française de l'Université de Montréal.

