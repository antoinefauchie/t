---
title: "L'édition : un acte technique"
type: "chapitre"
chapitre: 2
section: 0
url: "/chapitre-02"
bibfile: "data/02.json"
---

Le livre est un aboutissement visible et lisible d'un processus nommé édition.
L'édition est donc une série d'actions, de méthodes et un type d'organisation mis en place afin de produire notamment un artefact comme un livre.
Établir une définition de l'édition est une entreprise qui est facilitée par les recherches réalisées sur le livre dans le chapitre précédent{{< renvoi chapitre="1" section="0" >}}.
Comprendre quels sont les rouages et les mécanismes de l'édition, et comment elle évolue aujourd'hui, est un travail complexe et un enjeu décisif sur les questions de production et de circulation du savoir, et sur la conceptualisation de nouveaux modèles épistémologiques.
Notre hypothèse consiste en une définition de l'édition comme une activité hybride et réflexive, définition qui se distingue de celles auxquelles nous sommes habituellement confrontés.
Nous construisons notre réflexion à travers des théories de l'édition, que nous mettons en tension avec des études de cas d'initiatives contemporaines et singulières.

L'édition est un ensemble d'activités organisées autour de dispositifs techniques, elle s'inscrit dans une histoire longue des actions humaines.
L'édition constitue un processus dont la distinction de trois fonctions permet de l'établir en tant que concept.
En réponse à un travail de recherche et d'exploration historique, nous présentons une initiative contemporaine et expérimentale d'édition, Abrüpt, à travers ses mécanismes techniques et expérimentaux.
Cette étude de cas se veut une illustration d'une démarche originale d'édition qui se concentre sur les artefacts produits et les outils nécessaires à cette entreprise.
Étant donné des démarches éditoriales aussi fortes que celle d'Abrüpt — combinant des formes éditoriales inattendues et une occupation de l'espace numérique —, le concept d'édition est mis en perspective avec un second concept : l'éditorialisation.
Sa définition en sciences de l'information et en études des médias, ainsi que sa confrontation au concept d'édition, ouvre de nouvelles perspectives, et notamment celle d'un débordement des liens entre dispositifs techniques et visions du monde au-delà de la production de _livres_.
Pour confirmer ce concept, et considérer la technicité du dispositif littéraire qu'est l'édition, nous présentons et analysons la chaîne d'édition à l'origine des livres des Ateliers de \[sens public\] — dont un ouvrage a été présenté dans le chapitre précédent{{< renvoi chapitre="1" section="5" >}}.
Procédé technique, chaîne d'édition ou processus complexe, le _Pressoir_ est une réponse à des enjeux éditoriaux, et la construction d'un environnement pour penser de nouvelles modélisations conceptuelles.
Le _Pressoir_ est aussi une expression de l'édition d'aujourd'hui.

Après le livre comme artefact littéraire, c'est donc l'édition comme acte technique que nous étudions, avant d'aborder le numérique comme espace et comme moyen.

