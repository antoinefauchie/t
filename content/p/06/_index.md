---
title: "Conclusion"
type: "conclusion"
chapitre: 6
section: 0
url: "/conclusion"
bibfile: "data/06.json"
---

Les _fabriques d'édition_ sont des processus techniques qui fabriquent et produisent du sens, tant à travers les différents artefacts qui en conservent des traces, que dans les modélisations qui les précèdent.
Nous avons pu démontrer que ces _fabriques_ se constituent dans des situations où la technique est interrogée et critiquée.
C'est donc un désir de _faire autrement_, une volonté de remettre en cause les archétypes établis ou imposés, qui ouvre des perspectives vers de nouveaux modèles épistémologiques.

Notre hypothèse s'est constituée et a été argumentée autour de cinq grandes thématiques, qui ont chacune fait l'objet d'un développement conceptuel, d'un prolongement critique et de l'élaboration d'un nouveau concept.
Ces analyses et ces propositions théoriques ont été articulées puis fondées autour d'études de cas de dispositifs et d'expérimentations.
Le parcours général de cette thèse a été progressif, partant du livre comme artefact, étudiant l'édition comme acte et comme éditorialisation, présentant le numérique comme environnement puis comme processus technique avec l'édition numérique, analysant les formats comme dispositif de modélisation du sens, et questionnant enfin le logiciel pour conceptualiser la _fabrique_.

