---
title: "Éditer autrement : le cas de _Busy Doing Nothing_"
chapitre: 1
section: 3
bibfile: "data/01.json"
_build:
  list: always
  publishResources: true
  render: never
---

Quelles peuvent être les caractéristiques d'un livre d'aujourd'hui ?
Le duo Hundred Rabbits répond à cette question avec une aventure éditoriale qui illustre la définition d'un livre, en adoptant une démarche originale et en utilisant des procédés techniques actuels.
Si le résultat est un livre imprimé qui semble classique en tant qu'objet, il interroge les processus de fabrication et de production, il remet en cause des principes de légitimité éditoriale, et il dévoile également les liens qui existent entre plusieurs artefacts d'un même projet éditorial.
En dédiant une étude de cas à l'ouvrage _Busy Doing Nothing_, nous prolongeons la définition du livre en tant que concept, et nous observons combien son évolution est liée à celles des avancées techniques et technologiques.

{{< citation ref="bellum_busy_2021" >}}
In 2020, we completed our circumnavigation of The Pacific Ocean.  
The last passage from Japan (Shimoda) to Canada (Victoria) took 51 days, and it was the hardest thing we've ever done.
{{< /citation >}}


### 1.3.1. Un journal de bord édité

_Busy Doing Nothing_ {{< cite "bellum_busy_2021" >}} est un livre écrit et édité par Rekka Bellum et Devine Lu Linvega en 2020 pour les versions numériques et 2021 pour la version imprimée : il s'agit du journal de bord de leur traversée en bateau de l'océan Pacifique entre le Japon et le Canada entre juin et juillet 2020 pendant 51 jours.
D'abord écrit dans un carnet manuscrit, ce journal de bord a été transcrit sur le site web de Hundred Rabbits — le nom du duo que forme les deux auteurs — en 2020 {{< cite "rabbits_north_2020" >}} puis édité sous forme de livre numérique aux formats EPUB et PDF en février 2021.
Ces deux dernières versions comportent des ajouts significatifs : des corrections et des ajustements importants dans le texte, des illustrations supplémentaires, et une section avec des contenus additionnels — concernant la nourriture stockée sur le bateau et plusieurs recettes.
En décembre 2021 une édition imprimée est proposée, comportant les mêmes contenus que les versions PDF et EPUB.
Ce _livre_ existe ainsi sous deux versions différentes et quatre formats.
Une version originale, ou _non éditée_ : une page web {{< cite "rabbits_north_2020" >}} comportant la transcription du journal de bord avec quelques photos du voyage et du carnet manuscrit (pour les illustrations).
Une version augmentée en un texte original corrigé et augmenté, avec des contenus supplémentaires : au format EPUB : un livre numérique pour des liseuses à encre électronique ; au format PDF : un livre numérique paginé pour tout type de dispositif informatique ; enfin au format imprimé : un livre imprimé vendu via la plateforme d'impression à la demande Lulu.com.

{{< figure type="figure" src="busy-doing-nothing-01.jpg" legende="Photographie de la couverture de l'édition imprimée du livre _Busy Doing Nothing_" >}}

{{< figure type="figure" src="busy-doing-nothing-02.jpg" legende="Photographie des pages intérieures 45 et 46 de l'édition imprimée du livre _Busy Doing Nothing_" >}}

La version imprimée est un livre au format A5 et au dos collé d'environ 250 pages.
Trois sections constituent le texte : une introduction, le journal de bord avec une conclusion, des contenus additionnels avec la liste des mets conservés sur le bateau et des recettes.
La version EPUB conserve la même structure.

Ce projet éditorial a une autre spécificité : les sources de la version augmentée sont disponibles en tant que code sur un dépôt Git sur la plateforme SourceHut {{< cite "rabbits_north_2023" >}}, accessible librement.
L'objectif de cette mise à disposition des sources est de montrer comment le livre est fabriqué, de permettre à qui le souhaite de reconstruire le livre, mais aussi de recueillir des contributions si des erreurs sont détectées par les lecteurs et les lectrices.


### 1.3.2. Une démarche originale

Il faut expliquer ici la démarche globale de Rekka Bellum et Devine Lu Linvega, qui constituent le collectif Hundred Rabbits, ainsi que le processus d'édition.
Rekka Bellum{{< n >}}[https://kokorobot.ca/site/rek.html](https://kokorobot.ca/site/rek.html){{< /n >}} se définit comme écrivain et dessinateur, et Devine Lu Linvega{{< n >}}[https://wiki.xxiivv.com/site/devine_lu_linvega.html](https://wiki.xxiivv.com/site/devine_lu_linvega.html){{< /n >}} comme développeur, artiste et musicien.
Le duo vit sur un bateau de 10 mètres de long, et a adopté un mode de vie et de création adapté à cette contrainte : peu d'énergie disponible ; nécessité de pouvoir travailler pendant plusieurs jours ou semaines sans connexion internet ; sélectionner ou créer des outils résistants ; être autonomes vis-à-vis des plateformes centralisées pour développer, diffuser et sauvegarder leurs créations.
Ce mode de vie est radical, dans le sens où il est complet et total.
Les créations — programmes et contenus — créés par Hundred Rabbits sont le reflet de ces choix, elles donnent à voir un positionnement écologique, politique ou éthique.
Le livre _Busy Doing Nothing_ participe aussi de cette démarche de constituer un environnement dédié à la création tout en respectant ce mode de vie.

{{< citation ref="rabbits_mission_2021" >}}
We founded Hundred Rabbits with the goal of building a platform that could enable us to dedicate our time to the creation of free & open-source software such as Grimgrains (http://grimgrains.com) and orca (https://100r.co/site/orca.html), as well as contribute to the open-source projects of others.
{{< /citation >}}

Le duo adopte un positionnement politique global en faveur de la maîtrise de leur environnement de création et de l'ouverture, en s'engageant dans des communautés autour du logiciel libre ou _open source_ — à la fois à travers le développement de leurs projets personnels et aussi en contribuant à d'autres initiatives.
Cela pose la question de la soutenabilité économique d'une telle démarche, les modalités de ce que nous pourrions qualifier de _modèle économique_ sont décrites par la suite.

Le livre a été conçu en deux phases distinctes : la transcription du journal manuscrit sur le site web, puis l'édition de ce texte et la production des artefacts correspondants.
La page web présentant la transcription a été écrite directement au format HTML, ce document est versionné dans un dépôt Git sur la plateforme GitHub{{< n >}}[https://github.com/hundredrabbits/100r.co/blob/main/site/north_pacific_logbook.html](https://github.com/hundredrabbits/100r.co/blob/main/site/north_pacific_logbook.html){{< /n >}}, ce qui permet de naviguer dans l'historique des modifications voire de proposer des corrections.
L'aspect formel de cette version HTML est simple, les entrées du journal sont représentées sous forme de cellules d'un grand tableau.
Après cette page web, les deux formats numériques EPUB et PDF ont été produits simultanément, avec une approche sensiblement différente.
Une même source, un fichier au format Markdown, est à l'origine des deux fichiers.
L'édition a donc été réalisée dans cet espace de balises légères, et versionné avec Git : le dépôt GitHub nous apprend qu'il y a eu 77 commits entre décembre 2020 et novembre 2021 {{< cite "rabbits_north_2023" >}}.
La conversion de la source vers les formats EPUB et PDF est réalisée avec Pandoc, un convertisseur de langages de balisage que nous décrivons plus longuement par la suite{{< renvoi chapitre="4" section="4" >}}, ainsi que (Xe)LaTeX{{< renvoi chapitre="3" section="3" >}} pour la production du PDF, et Calibre pour la conversion du fichier EPUB au format MOBI — un format proche de l'EPUB mais pour les liseuses Kindle d'Amazon.
La version PDF téléchargeable en ligne — commercialisée, voir plus loin — est la même que la version imprimée disponible sur la plateforme Lulu.com.
Le format PDF pour la version imprimée suit le même processus de production.
Les versions numériques sont produites selon les principes du _single source publishing_, soit le fait de produire plusieurs artefacts à partir d'une source unique{{< renvoi chapitre="4" section="4" >}}.
Une correction dans le fichier source est donc reportée dans tous les fichiers de sortie.

Les outils de fabrication choisis pour ce travail éditorial sont à l'image de la démarche globale du collectif Hundred Rabbits, simples, maîtrisés, dépouillés, résilients.
L'affirmation "Go slow and fix things" se retrouve sur plusieurs des sites et plateformes des deux artistes.
Le collectif Hundred Rabbits n'utilise pas les logiciels habituels, tels que des logiciels de publication assistée par ordinateur comme InDesign, préférant maîtriser les différents programmes nécessaires à la fabrication et — en partie — à la production des différents artefacts.
Lors d'une conférence en novembre 2022 Devine explique ce positionnement par rapport aux outils en général et à l'informatique en particulier :

{{< citation ref="rabbits_weathering_2023" lang="en" >}}
As a disclaimer, all that I am writing now is very naive. I draw, and I make music, when I started doing research I didn't have the vocabulary to find what I was looking for. I didn't know what virtual machines were, I didn't know what compilers were either. I had a vague idea of what programming was. I had written Swift and Objective C, but had no conception of how it actually tied to processors. It seemed like I was learning a service, the same way I was learning "To Photoshop". It wasn't like learning a skill, you don't learn to draw when you use Photoshop, you learn how to operate within the confines of someone else's playground, and when that rug is pulled from underneath you, there's nothing you can say or do, and you never really understood how it worked in the first place so you can't really replicate it.
{{< /citation >}}

Hundred Rabbits opère un changement de paradigme en refusant de recourir à des "services" et en construisant la majorité des outils utiles à leurs créations, ou en tout cas en acquérant une maîtrise des logiciels et des programmes dûment sélectionnés.
Les efforts nécessaires pour parvenir à ce degré d'autonomie sont importants, et nous pouvons noter ici que cela implique au moins des compétences spécifiques, un temps dédié à l'apprentissage des différentes briques technologiques, et une curiosité considérable.
Ces éléments sont compatibles avec le mode de vie adopté par le duo, et semblent difficilement envisageables dans le cas d'une structure classique — comme une maison d'édition.

Ce livre est auto-édité, ce sont en effet les mêmes personnes qui écrivent, éditent et diffusent l'ouvrage dans ses différentes formes.
Les versions numériques sont vendues via la plateforme Itch.io, et la version imprimée via la plateforme Lulu.com, mais la publication ainsi que la diffusion dépendent entièrement de Rekka Bellum et de Devine Lu Linvega.
Cette démarche autonome repose sur plusieurs prérequis : le duo dispose d'une communauté active qui suit leurs activités via différents canaux ; Hundred Rabbits dispose de ses propres outils de communication comme son site web, sa lettre d'information ou une présence sur des réseaux sociaux (principalement le réseau fédéré Mastodon en 2023) ; et ils sont présents sur Patreon, plateforme de financement sur laquelle plus de 200 personnes contribuent financièrement à leurs projets (avec ou sans contrepartie).
L'option de l'auto-édition, dans ce contexte, se comprend amplement : Hundred Rabbits dispose des outils de diffusion et de communication pour que le livre puisse être connu et acheté, et il n'y a pas de volonté de leur part d'intégrer les circuits classiques du livre — en accord avec leur position politique déjà évoquée.
Hundred Rabbits est en soi déjà une structure d'édition, certes plus habituée au jeu vidéo qu'au livre, mais qui a construit sa légitimité depuis plusieurs années.
Cette légitimité est une légitimité _au-dessus_ pour reprendre la catégorisation d'Emmanuel Cardon {{< cite "cardon_a_2015" >}}, puisqu'elle est construite avec des communautés identifiées qui permettent au collectif Hundred Rabbits d'être reconnu, de faire autorité{{< n >}}Ce terme lui-même ne serait pas reconnu comme légitime par le collectif, tant les deux artistes construisent ou contribuent à des communautés qui ne reposent pas sur des principes verticaux.{{< /n >}}.
Pour prendre la mesure de cette légitimité nous pouvons observer trois facteurs quantitatifs : le nombre de personnes abonnées aux comptes Mastodon de Devine Lu Linvega (11 400 en novembre 2023) et de Rekka Bellum (4 700 en novembre 2023), le nombre de personnes qui contribuent à leur Patreon (460 en novembre 2023, pour un revenu total de 1621$CAD par mois au même moment), et le nombre de personnes abonnées à leur profil Itch.io (plateforme sur laquelle ils mettent à disposition la majorité de leur création).
La démarche d'indépendance se comprend mieux en prenant en compte l'activité première du collectif, en effet le domaine du jeu vidéo est habitué depuis longtemps à se constituer autant en grandes structures qu'en petits îlots indépendants.
Il y a ici une influence, voire une transposition, des pratiques dans le domaine du jeu vidéo _indépendant_, voir du développement de logiciels libres, vers l'édition.


### 1.3.3. Est-ce un livre ?

Ce livre est-il un livre ?
Nous posons cette question en raison de l'originalité de la démarche, comparée à des pratiques d'édition plus classiques.
Ce livre semble répondre, de façon formelle, à la définition que nous avons établie{{< appel identifiant="livre" >}} : un artefact éditorial clos, résultat d'un travail d'écriture et d'édition, de création ou de réflexion, un objet physique (par exemple imprimé) ou numérique (un fichier ou un flux) maniable voire malléable.
Ces artefacts ne présentent pas de spécificités particulières, si ce n'est le goût de la simplicité, autant dans les versions/formats papier ou numérique, et disposent de formes classiques.
Pourtant le livre se situe à la marge des espaces de publication majoritaires : il ne contient pas d'ISBN, il n'est référencé dans aucune base de données de librairies, aucun catalogue de bibliothèque, et non plus sur des plateformes du type Amazon.
Ce livre n'est identifiable que via les canaux de communication de Hundred Rabbits, ou très bien référencé sur la plateforme Itch.io.
Autant dire que l'objet éditorial se démarque des initiatives habituelles par son affirmation d'être diffusé autrement.

Nous constatons que Hundred Rabbits n'a tout simplement pas besoin de référencer ailleurs ce livre (et ses artefacts).
D'une part parce que le domaine du livre est probablement un _écosystème_ moins connu des deux protagonistes et que l'investir serait particulièrement chronophage, mais aussi parce qu'il est question de choix politique et de cohérence.
Rekka Bellum et Devine Lu Linvega adoptent des pratiques totalement imbriquées dans des principes de création, voire de vie.
Le choix éditorial, fort, comprend autant ces questions de diffusion que les outils utilisés pour _fabriquer_ puis _produire_ le livre.
Diffuser soi-même un texte participe d'un même élan, élan créatif — qui peut aussi être qualifié de littéraire —, qui consiste également à refuser certains codes, notamment le fait de devoir être référencé en librairie, ou encore un fonctionnement basé sur un mode de rentabilité.
Hundred Rabbits construit d'autres conditions pour fabriquer et diffuser des livres.

_Busy Doing Nothing_ est un livre d'aujourd'hui.
Il porte la trace de sa fabrication, de l'intention éditoriale comme nous l'analysons dans un prochain chapitre{{< renvoi chapitre="2" section="2" >}}.
Si nous considérons les quatre artefacts que sont la page web, le fichier EPUB, le fichier PDF et le livre imprimé, nous pouvons comprendre comment ce livre a été pensé, quel a été l'acte éditorial de Rekka Bellum et de Devine Lu Linvega.
Le concept du livre, jusqu'ici considéré comme objet dans un écosystème, doit être confronté à un autre concept, plus proche des questions de fabrication et de technique : l'artefact.

