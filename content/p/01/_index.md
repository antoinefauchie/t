---
title: "Le livre, cet artefact éditorial"
type: "chapitre"
chapitre: 1
section: 0
bibfile: "data/01.json"
url: "/chapitre-01/"
---

Le livre est un objet culturel et industriel.
Son caractère commun dissimule des processus intellectuels, techniques et économiques complexes.
Que nous révèlent une définition approfondie du livre et une description des procédés techniques qui sont à son origine ?
Le livre est le résultat de modes de production du savoir, son étude nous permet à la fois d'identifier des modélisations épistémologiques et d'établir le lien entre la technique et un dispositif de production et de diffusion de la connaissance.
Le livre, en tant qu'aboutissement d'une _chaîne_, porte ses conditions d'existence, il est un _artefact_.

Nous confrontons plusieurs considérations du livre — fonction, concept et aspects — en explorant certains des événements historiques importants des modes de reproduction de cet objet paginé.
Ainsi, nous établissons un lien entre les évolutions du livre et celles de la technique.
La circulation du savoir est liée aux formes du livre, l'analyse de certaines d'entre elles nous amène à distinguer fabrication et production, et à refuser une dualité entre _contenant_ et _contenu_.
Les livres qui se démarquent par leur forme expriment des modalités spécifiques de production et de diffusion de la connaissance.
C'est pourquoi nous nous intéressons à _Busy Doing Nothing_ de Rekka Bellum et Devine Lu Linvega, un ouvrage dont la forme et ses formats questionnent la façon dont un livre peut être conçu et édité dans une perspective ouverte et expérimentale.
À partir de cette étude de cas et de nos recherches théoriques sur une définition conceptuelle du livre, nous sommes en mesure de considérer le livre comme un artefact.
Il s'agit alors d'affirmer les liens entre livre et technique, la définition du livre dépendant des conditions de sa fabrication et de sa production.
L'étude de cas d'une expérimentation à laquelle nous avons contribué, l'ouvrage _Exigeons de meilleures bibliothèques_ de R. David Lankes publié aux Ateliers de \[sens public\], illustre et épuise le concept d'_artefact éditorial_, en tant que mode de production d'un objet culturel qui conserve les marques d'un processus technique de fabrication, et donne également à voir une pratique non conventionnelle d'édition.

