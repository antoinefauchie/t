---
title: "Le numérique et l'avènement de l'édition numérique"
type: "chapitre"
chapitre: 3
section: 0
url: "/chapitre-03"
bibfile: "data/03.json"
---

Le livre et l'édition constituent respectivement des artefacts énonciateurs et des processus dispositifs, dont l'étude nous permet de comprendre l'émergence du sens et les conditions de la diffusion de la connaissance.
Nous analysons désormais le rôle du numérique en tant modification profonde des mécanismes de modélisation épistémologique.
Nous avons déjà consacré plusieurs développements pour définir l'environnement numérique et ses dynamiques, notamment autour de l'éditorialisation{{< renvoi chapitre="2" section="4" >}}, il s'agit maintenant de définir le numérique en tant que reconfiguration des processus eux-mêmes.
Comment s'établissent des chaînes ou des _fabriques_ d'édition avec le numérique ?
Qu'est-ce que signifie éditer _en_ numérique ?
C'est grâce au travail de définition et de conceptualisation du livre et de l'édition que nous pouvons désormais présenter, analyser et critiquer le numérique.

Nous définissons tout d'abord le numérique et son écosystème, en tant que mode de représentation et de diffusion d'information, en réseau, dont l'usage des protocoles et des standards issus de l'informatique forme _des_ cultures numériques.
Dans le domaine de l'édition, le numérique a engendré un nouvel objet, le livre numérique, qui révèle un mode de fonctionnement ou d'adoption bien particulier de duplication du modèle imprimé, que nous qualifions d'_homothétisation_.
Cette disposition représente une phase intermédiaire de compréhension et d'intégration des enjeux numériques.
Pour expliquer ce que signifie éditer _avec_ le numérique, tout en préservant une longue histoire typographique et bibliographique, la première étude de cas de ce chapitre est consacrée à Ekdosis, un paquet LaTeX pour l'édition critique.
Cette étude de cas nous amène à interroger le lien fondateur entre l'édition et les humanités numériques.
Les sciences humaines et sociales ont développé, avec les humanités numériques, une approche et une méthodologie critique fortement ancrées dans des pratiques d'édition et de publication numériques.
Enfin nous dédions la seconde étude de cas à un projet qui bénéficie des méthodologies initiées par les _digital humanities_ tout en se situant dans sa marge.
Les modes de fabrication qui y sont développés incarnent une définition possible de l'_édition numérique_, implémentant des méthodes et des principes issus des mouvements critiques des humanités numériques.

Cet examen approfondi du numérique, dans le prolongement d'une étude des processus d'édition, nous permet d'interroger la question des formats, ce que nous faisons dans le chapitre suivant.

