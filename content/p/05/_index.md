---
title: "Éditer avec les fabriques"
type: "chapitre"
chapitre: 5
section: 0
url: "/chapitre-05"
bibfile: "data/05.json"
---

Après le livre, l'édition, le numérique et le format, il s'agit désormais d'appréhender les processus d'édition classiques en tant qu'ils sont un assemblage de logiciels et de programmes, de pratiques incarnées par des personnes, et de méthodes et de protocoles, afin de prolonger l'analyse de processus d'édition non conventionnels en tant qu'ils peuvent être une imbrication du façonnage de processus et de textes.
Ce que nous nommons _fabrique_ et que nous développons en tant que concept est ce double mouvement entremêlé de travail sur des textes et de constitution de processus techniques.

Une _fabrique d'édition_ est un processus technique et dispositif qui comprend autant l'édition de textes que la mise en place ou l'agencement d'outils permettant ce travail d'édition.
Fabriquer des éditions, éditer des fabriques, nous avons déjà observé ce phénomène dans plusieurs initiatives telles qu'exposées dans les précédents chapitres.
Le concept de _fabrique_ rassemble plusieurs principes que nous détaillons ici, nous explicitons comment ils s'incarnent dans les concepts et projets qui suivent.
En plus du double mouvement d'édition de textes et de construction de chaînes, il s'agit aussi de dévoiler et de révéler les rouages des processus et de comprendre l'architecture technique et épistémologique de ces composants ou micro-programmes.
Dans le champ de l'édition, comprenant dans ce chapitre les métiers du design graphique, nous observons le développement de méthodes et d'outils qui ne reposent pas sur les logiciels.
Les acteurs et les actrices de pratiques d'édition qui adoptent des démarches _logiciellement_ décentrées — et donc qualifiées de non conventionnelles — remettent régulièrement en cause les outils majoritairement utilisés ainsi que ceux qu'elles ont elles-mêmes créés, le choix est fait d'une plus grande malléabilité et découvrabilité.
Ce phénomène comprend aussi une dimension réflexive et critique, les pratiques sont en effet interrogées dans leurs dispositions techniques au moment de leur mise en place.
Il ne s'agit pas seulement d'avoir recours à des logiciels _alternatifs_, mais de sortir de la rhétorique des _solutions_ techniques.

Quelle est l'origine de cet entremêlement des actes d'édition et de la création de chaînes d'édition ?
À quel point un acte d'édition peut-il également être une opération de construction d'un processus ?

Pour répondre à ces questions nous définissons d'abord ce qu'est le logiciel, en tant qu'objet qui s'est constitué avec l'informatique puis le numérique, en analysant le changement de paradigme induit.
Le logiciel, tel que nous le connaissons aujourd'hui, est, d'une certaine façon, l'_anti-fabrique_.
Il y a toujours eu des pratiques d'écriture et d'édition — au sens large — sans logiciel, mais quelles sont-elles ?
Pour les expliciter nous observons deux moments de l'histoire de l'informatique et du numérique, d'un côté les débuts des interfaces graphiques dite utilisateur, et de l'autre l'émergence du mouvement dit du _CSS print_.
Pourquoi vouloir se débarrasser du logiciel qui semble pourtant faciliter le travail éditorial ?
C'est l'objet de l'étude de cas sur C&F Éditions, où des livres sont fabriqués sans logiciels.
Nous définissons enfin le concept de _fabrique_ et le phénomène de _fabrique d'édition_, en analysant les approches théoriques de Tim Ingold et de Vilèm Flusser, et en puisant dans les précédents chapitres.
Pour expliquer ce double mouvement où des processus d'édition sont créés en même temps que des textes sont édités, nous réalisons une double étude de cas autour d'une _fabrique d'édition_ multi-formats et d'une autre multimodale, les deux étant des expérimentations réalisées dans le cadre de cette thèse.

