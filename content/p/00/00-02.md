---
title: "Situer notre recherche"
chapitre: 0
section: 2
bibfile: "data/00.json"
_build:
  list: always
  publishResources: true
  render: never
---

Ce travail de recherche sur l'édition s'inscrit dans plusieurs champs, cette pluridisciplinarité forme une convergence.

Notre étude d'objets éditoriaux et de structures d'édition ancre notre recherche dans le vaste champ littéraire {{< cite "bourdieu_les_1998" >}}.
Nous analysons plus spécifiquement des dispositifs qui articulent texte et technique {{< cite "archibald_texte_2009" >}} et qui sont à l'origine d'objets culturels qui forment la littérature, objets considérés comme des _matérialités_ {{< cite "chartier_inscrire_2005" >}}.
Les sciences de l'information et de la communication constituent une diversité de théories qui analysent le rôle joué par les processus d'information et de communication sur l'espace social, économique et politique {{< cite "avila_araujo_epistemologie_2022" >}}.
Il s'agit plus spécifiquement d'étudier l'édition en tant que dispositif d'information, et ainsi de convoquer des théories comme l'énonciation éditoriale {{< cite "souchier_image_1998" >}} ou l'éditorialisation {{< cite "vitali-rosati_pour_2020" >}}, ou encore d'aborder le concept de chaînes d'édition {{< cite "crozat_chaines_2012" >}}.

Les études du livre et de l'édition se consacrent à des phénomènes globaux tels que la constitution du savoir à travers l'histoire de l'édition {{< cite "sordet_histoire_2021" >}}, considérant le livre comme un _moteur de l'histoire_ {{< cite "febvre_apparition_1957" >}}.
Ces études observent et analysent les évolutions historiques et sociales auxquelles l'édition contribue {{< cite "eisenstein_printing_1979" >}}, ainsi que les enjeux économiques autour du livre {{< cite "mollier_ou_2007" >}}.

<div class="break"></div>

Le numérique constitue un terrain de recherche pour ces études de l'édition et pour les sciences de l'information et de la communication, permettant de traiter des questions centrales comme l'auctorialité {{< cite "broudoux_editorialisation_2022" >}}, ou comme l'apparition et la transformation de _métiers_ et de _pratiques_ {{< cite "zacklad_espace_2007" >}}, plus spécifiquement dans l'édition numérique {{< cite "sinatra_pratiques_2014" >}}.
Les études des médias nous permettent de disposer d'outils théoriques pour interroger le livre et analyser ses évolutions dans une perspective de (re)médiation {{< cite "bolter_remediation:_2000" >}}.
Les médias sont des structures de communication qui reposent sur des formes technologiques et leurs protocoles {{< cite "gitelman_always_2006" "7" >}}.
En considérant le livre comme un média, et l'édition comme étant à son origine, nous relevons les tensions inhérentes à l'étude de ces objets.

Enfin, nous adoptons l'approche des humanités numériques qui contribue à constituer une méthodologie articulant recherches théoriques, expérimentations pratiques, et regards réflexifs et critiques {{< cite "burdick_digital_humanities_2012" >}}.
Nous considérons que les humanités numériques se sont en grande partie fondées sur des pratiques d'édition {{< cite "mounier_readwrite_2012" >}}, et que les enjeux de publication restent au cœur de cette approche.
La propension à construire les outils nécessaires à une recherche scientifique contemporaine caractérise également l'approche dite des _digital humanities_ {{< cite "ramsay_building_2016" >}}.
Nous nous plaçons clairement dans cette tendance, ne pouvant pas envisager une pratique académique sans participer à l'élaboration des processus techniques d'émergence et de production du sens, et à des environnements collectifs de fabrication critique, qui relèvent d'un nouveau type d'herméneutique.

