---
title: "Introduction"
type: "introduction"
chapitre: 0
section: 0
url: "/introduction"
bibfile: "data/00.json"
---

Cette thèse porte sur les processus d'édition : elle défend l'hypothèse qu'ils sont constitutifs de la production du sens et qu'ils reflètent des visions du monde plurielles.
Nous considérons le phénomène de _fabrique d'édition_, dans lequel des dimensions techniques sont imbriquées, telles que la construction de procédés de fabrication et de production de formes, d'objets et d'artefacts que sont les livres, ou tel que le travail sur le texte, comme l'architecture des contenus, la structuration sémantique et la composition typographique.
L'acte éditorial comprend autant la formalisation d'un texte que la constitution des outils permettant ce travail.

