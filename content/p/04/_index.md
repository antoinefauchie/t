---
title: "Les formats, structuration et modélisation du sens"
# Les formats : entre fondements de l'édition et modélisation du sens
resume: "Les formats représentent les énonciations, les actes ou les contraintes dont l'édition est l'objet. Nous nous concentrons ici sur les enjeux sémantiques des formats en tant qu'ils résultant ou permettent un acte éditorial en contexte numérique. Comment s'incarne le sens dans une activité d'édition ? Les formats posent des enjeux épistémologiques qu’il convient d’analyser via l'étude d'un type de format, le format texte, permettant de produire une sémantique via des langages dits de balisage. C'est également dans une perspectivre de publication multimodale à partir d'une source unique que nous explorons des processus de modélisation basés sur ces formats."
type: "chapitre"
chapitre: 4
section: 0
url: "/chapitre-04"
bibfile: "data/04.json"
---

Après le livre, l'édition et le numérique, nous plongeons plus profondément dans les rouages des processus d'édition — ou la _fabrication_ d'artefacts éditoriaux — avec la question des formats.
Ce chapitre est consacré à la technique éditoriale, considérant que les formats représentent les énonciations, les actes ou les contraintes dont l'édition est l'objet.
Comment s'incarne le sens dans une activité d'édition ?
Les formats représentent des enjeux épistémologiques qu'il convient d'analyser.
Benoît Epron et Marcello Vitali-Rosati pointent à juste titre l'enjeu d'une compréhension et d'une maîtrise des formats dans l'édition :

{{< citation ref="epron_ledition_2018" page="35" >}}
Les technologies déployées aujourd'hui dans l'édition numérique ont en effet avant tout été développées pour répondre aux besoins de l'informatique d'abord et du Web ensuite. Il s'agit d'un renversement important, puisqu'une part incontournable de l'activité éditoriale devient de fait conditionnée par des choix techniques issus de secteurs d'activité parfois très éloignés de l'édition. Les conséquences de cette situation sont doubles : d'une part, la mise au point et l'adoption des standards techniques du métier ne relèvent plus uniquement d'acteurs du monde de l'édition ; d'autre part, les éditeurs sont contraints de composer avec des formats ou des technologies qui ne correspondent pas nécessairement à leurs enjeux.
{{< /citation >}}

Nous avons déjà évoqué la question des formats à plusieurs reprises, mais sans détailler ce que leur origine, leur fonctionnement, leur modélisation ou leur usage impliquent.
D'un format à l'autre, l'objectif est de modeler et de convertir des textes pour aboutir à des artefacts.
Dans une perspective des études des médias, les formats représentent des enjeux trop importants pour être ignorés, comme le soulignent Axel Volmar, Marek Jancovic et Alexandra Schneider dans l'introduction du recueil _Format Matters: Standards, Practices, and Politics in Media Cultures_ {{< cite "jancovic_format_2019" "7-22" >}}.
Nous n'analysons pas en détail les formats classiques de l'édition, nous nous concentrons plutôt sur les enjeux sémantiques des formats dans une perspective numérique.

Nous définissons tout d'abord ce qu'est un format, autour de sa dimension technique et des notions d'_instructions_, de _formalisation_ ou de circulation de l'information.
Dans un environnement numérique les formats sont la condition d'une interopérabilité via l'établissement de standards.
Dans le domaine de l'édition, les formats ont aussi la charge de définir les modalités sémantiques nécessaires à l'édition numérique.
Dans un deuxième temps nous décrivons un type de format, le format texte, grâce auquel un balisage sémantique peut être implémenté.
Nous dédions une étude de cas à un langage de balisage léger particulier, Markdown, pour comprendre l'essor de pratiques d'édition spécifiques autour des principes du _single source publishing_.
Ces principes sont décrits dans une quatrième section, articulée autour d'une analyse, d'une critique et de perspectives de conceptualisation.
Enfin, une étude de cas sur le module d'export de l'éditeur de texte sémantique _Stylo_ vient expliciter la question de l'application des principes du balisage et de la publication multimodale à partir d'une source unique, et plus spécifiquement dans le domaine de l'édition scientifique.

