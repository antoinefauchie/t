---
title: "_Stylo_ et son module d'export : fabriquer des livres"
chapitre: 4
section: 5
bibfile: "data/04.json"
_build:
  list: always
  publishResources: true
  render: never
---

Quelles sont les modalités d'application des principes du _single source publishing_ en situation réelle telle que l'édition scientifique ?
Comment mettre en place un processus d'édition respectant ces principes et quels en sont les agencements le cas échéant ?
Cette étude de cas répond à ces deux questions en analysant les fonctionnalités d'export de l'éditeur de texte sémantique _Stylo_.
Comme toutes les études de cas qui ponctuent chacun des chapitres de cette thèse, celle-ci révèle l'implication de l'auteur dans un projet de recherche.
Ce dernier est ambitieux et, d'une certaine façon, radical.
La coordination des développements de _Stylo_ a structuré le doctorat pendant quatre années, poursuivant un travail engagé puis poursuivi par d'autres.
La dimension collective est ici primordiale, tant les idées, les décisions et les réalisations ont fait l'objet d'échanges avec les personnes qui ont contribué au projet — étudiants et étudiantes, chercheurs et chercheuses, éditeurs et éditrices de revues, et ingénieurs.
En plus d'expliciter l'usage du format Markdown dans une chaîne d'édition complexe relevant du domaine académique, et de détailler l'implémentation d'un acte d'édition sémantique, cette étude de cas répond à un troisième objectif : montrer comment l'édition et la modélisation d'un processus éditorial sont liées et se répondent.
Nous faisons un pas de côté en analysant une _fabrique_ d'articles plutôt que de livres, toutefois l'usage de cet éditeur de texte s'étend aussi à d'autres objets éditoriaux comme nous l'évoquons dans cette analyse.

Plusieurs projets s'inspirent ou intègrent les principes du _single source publishing_, dans des contextes éditoriaux variés.
L'implémentation de ces principes comporte des choix qui sont aussi parfois des compromis, notre objectif ici est donc aussi de nuancer ce qui semble être un horizon presque utopique, tout en proposant des voies alternatives aux initiatives le plus souvent orientées vers XML.

Pour analyser le fonctionnement de ce que nous nommons le _module d'export_ de _Stylo_, il faut tout d'abord préciser le contexte de la publication scientifique et ses particularités, puis présenter l'éditeur de texte sémantique _Stylo_ et ses origines théoriques et pratiques.
Le module d'export de _Stylo_ est analysé en tant que brique logicielle basée sur le convertisseur Pandoc, nous mentionnons ici également ce qui a présidé à son développement initial et ce qui anime ses futures évolutions.
Enfin, nous abordons les enjeux techniques et théoriques liés à la production d'articles scientifiques et de livres, nous concentrant sur la _modélisation_ de ces artefacts.


### 4.5.1. Le contexte de l'édition savante

L'édition scientifique, ou plus globalement l'éditions savante, est un domaine avec de nombreuses particularités, qui se distingue ainsi fortement de l'édition généraliste — littératures, romans, essais, livres pratiques, etc.
Cette distinction se fait sur plusieurs plans, et notamment les types de contenus, leur circulation dans le processus d'édition, les types de formats, ou encore les enjeux de diffusion.

La richesse des contenus des objets éditoriaux savants, d'un point de vue sémantique, rejoint la diversité des formats.
En effet, le texte et son matériel critique sont représentés dans des artefacts divers comme des articles, des chapitres, des ouvrages ou des textes d'actes de conférences, répondant à un ensemble d'exigences variées.
Les notes de bas de page, les références bibliographiques, les bibliographies, les citations longues, les figures, les index, etc., constituent une diversité et une abondance structurelles que l'imprimé a su intégrer à la _page_ — à un niveau de complexité granulaire encore plus élevé avec les éditions critiques comme nous l'avons vu précédemment{{< renvoi chapitre="3" section="3" >}}.
La transposition en environnement numérique oblige à une modélisation qui se révèle complexe, et qui a donné lieu à des schémas XML dont l'objectif est d'identifier avec justesse les types de fragments qui composent le texte scientifique.
Il s'agit là de pouvoir répondre aux _nouvelles_ contraintes de diffusion, où chaque document doit être rendu disponible sur diverses plateformes en ligne, en flux et en version paginée, et parfois aussi au format imprimé.

Le numérique vient ainsi bouleverser les modalités de diffusion de la connaissance, et notamment via les questions de stabilité et de citabilité — comment citer un passage précis d'un article disponible sous forme de page web plutôt qu'imprimée ? {{< cite "bleier_how_2021" >}} —, sous l'influence des besoins de diffusion en contexte numérique.
Pourtant, c'est bien le modèle de l'imprimé qui guide encore les modes de production, dont la page semble être un canon difficilement contournable {{< cite "grafton_page_2012" >}}.
Les outils d'écriture les plus utilisés appuient ce paradigme, les traitements de texte ne permettant pas d'aboutir à un modèle alternatif, notamment basé sur des documents richement structurés et dont la mise en forme est de fait dans les mains de l'éditeur.
Même des auteurs et des autrices qui travaillent avec une matière bien moins structurée que celle de l'édition savante le remarquent dès les débuts de l'hégémonie de Microsoft Word {{< cite "kirschenbaum_track_2016" >}}, et c'est également la critique adressée par Edward Tufte au logiciel Microsoft Powerpoint {{< cite "tufte_beautiful_2006" "156-185" >}}, qui partage le même modèle que Word.
Il y a une nécessité à repenser les modalités d'écriture et d'édition, pour inclure les possibilités offertes par le numérique en termes de structuration sémantique, de travail collectif, ou de conversation scientifique {{< cite "sauret_design_2018" >}}.

Parmi d'autres questions, le projet Revue2.0{{< n >}}[https://revue20.org](https://revue20.org/){{< /n >}} a répondu à celle-ci : "comment embrasser le numérique pour augmenter la qualité des artefacts éditoriaux et faciliter leur circulation en contexte scientifique ? Autrement dit, comment adapter les chaînes d'édition ?"
Ce projet, mené par la Chaire de recherche du Canada sur les écritures numériques de 2018 à 2021{{< n >}}L'auteur a participé au projet d'abord en tant que coordinateur d'expérimentations et de prototypes de 2019 à 2020, puis comme coordinateur du projet de 2020 à 2021. Revue2.0 a été financé par le Conseil de recherches en sciences humaines du Canada.{{< /n >}}, a permis de mettre en place différentes méthodes et outils suite à une série d'entretiens, d'ateliers et d'expérimentations — un chapitre de la thèse de Nicolas Sauret est consacré à ce projet {{< cite "sauret_revue_2020" "147-240" >}}.
Si la mission de fond était celle d'un accompagnement à la transition numérique, il s'agissait aussi d'expliquer sur un double plan théorique et pratique ce que les structures éditrices de revues savantes pouvaient ou devaient faire du numérique.
Le projet Revue2.0 n'est pas le seul à avoir amorcé cette réflexion, nous pouvons notamment citer le rapport _Mind the Gap_ dirigé par John Maxwell, où est réalisée une analyse panoramique des outils et des plateformes de publication _open source_ {{< cite "maxwell_mind_2019" >}}.
Cette recherche rejoint celle de la Chaire de recherche du Canada sur les écritures numériques, et partage la position : toute démarche scientifique doit être basée sur des outils ouverts — voire libres —, et sur des standards.
Nous ne citons pas ici d'autres initiatives académiques proche de ces démarches, elles sont néanmoins nombreuses, et s'inscrivent parfois dans le courant des humanités numériques.

L'éditeur de texte sémantique _Stylo_ prend une place centrale au sein du projet de recherche Revue2.0, permettant d'expérimenter la rédaction sémantique, l'évaluation (ouverte, semi-ouverte ou fermée), l'indexation par mots-clés contrôlés, ou encore une chaîne de publication multimodale à partir d'une source unique.
Présentons désormais cet éditeur de texte qui fait figure d'exception dans le paysage académique global, d'autant qu'il s'inscrit comme une alternative parmi d'autres démarches de niche — pour ne pas dire marginales.


### 4.5.2. _Stylo_ : un éditeur de texte sémantique

_Stylo_ est un éditeur de texte sémantique pour l'édition scientifique en sciences humaines et sociales, conçu par et pour la communauté scientifique.
Cet outil d'écriture et d'édition a pour objectif de transformer le flux de travail numérique des revues savantes, notamment en adoptant un mode WYSIWYM et en privilégiant ainsi d'abord le sens du texte avant son rendu graphique.
Ses fonctionnalités sont centrées autour du texte et de la dimension sémantique qu'il peut (doit) revêtir dans une perspective de publication académique.

{{< citation ref="debouy_stylo_2022" >}}
Stylo est un éditeur de texte en ligne conçu spécifiquement pour les sciences humaines et sociales. Il doit permettre de rédiger tout type de textes scientifiques (articles, monographies, thèses, mémoires, ouvrages collectifs, et théoriquement éditions critiques). Visant à combiner les bonnes pratiques de l’édition scientifique et celles de l’édition web […]. Le projet Stylo est né de la volonté de donner aux auteurs la maîtrise de leurs données scientifiques et de l’ensemble de la chaîne éditoriale.
{{< /citation >}}

Malgré les efforts de stabilisation de cet outil permise par la très grande infrastructure de recherche Huma-Num, _Stylo_ est un projet de recherche et a vocation à le rester, ce qui signifie qu'il doit être un moyen d'expérimenter des théories et non devenir un _produit_ au service d'une communauté — en tout cas tant qu'il est mené et coordonné par un laboratoire de recherche.
Ce positionnement est loin d'être simple, mais il s'inscrit ainsi dans une perspective scientifique revendiquée par l'équipe qui coordonne ce projet {{< cite "vitali-rosati_ecrire_2020" >}} — dont l'auteur de cette thèse fait partie.

_Stylo_ est une application en ligne, soit un site web disponible avec une connexion internet et permettant un mode interactif à la façon d'autres éditeurs en ligne.
L'avantage est qu'aucune installation de logiciel n'est requise — si ce n'est un navigateur web —, en revanche une connexion internet fiable est nécessaire.
Les utilisatrices et les utilisateurs ne sont toutefois pas _enfermés_ avec cet éditeur puisqu'il est possible d'extraire les fichiers sources de l'application, ces fichiers étant des formats standards lisibles et modifiables par d'autres logiciels.
L'unité documentaire adoptée est l'article, soit un document comprenant un texte, un matériel critique et des métadonnées descriptives, ce qui n'empêche pas de publier aussi des monographies.
En plus d'une interface d'écriture qui n'affiche que le balisage sémantique — choix fort qui correspond à un autre paradigme que celui des traitements de texte —, _Stylo_ permet de prévisualiser une version web du document.

Le fonctionnement de _Stylo_ peut être décrit selon une approche sémantique et plus précisément en explicitant le rôle et les formats (standards et ouverts) des sources nécessaires à l'écriture et à l'édition : Markdown, YAML, BibTeX.
La première source est le texte, son format est le langage de balisage léger Markdown — saveur CommonMark/Pandoc, pour faire écho à l'étude de cas précédente{{< renvoi chapitre="4" section="3" >}} —, utilisé ici comme écriture sémantique.
Les niveaux de titres, notes, citations, emphases, tableaux et autres listes sont donc exprimés avec les signes typographiques propres à Markdown.
Les références bibliographiques sont quant à elles indiquées via une syntaxe propre au convertisseur Pandoc — ce fonctionnement est détaillé par la suite.
Pour structurer les métadonnées propres à l'article, _Stylo_ utilise le langage de sérialisation de données YAML, format répandu et souvent utilisé en complément de Markdown.
Ici les métadonnées sont stockées dans un fichier distinct du texte, c'est la deuxième source utilisée par l'éditeur.
La troisième et dernière source est la bibliographie structurée, au format BibTeX.
Chaque référence est décrite dans ce format de sérialisation de données, assez proche de YAML dans l'esprit.
Le texte, ses métadonnées et des références bibliographiques structurées, voilà sur quoi se base _Stylo_.
Dit autrement, _Stylo_ n'est qu'une couche interfacielle facilitant l'interaction avec ces fichiers, et proposant par ailleurs des fonctionnalités d'export permises par Pandoc et que nous explorons par la suite.

En tant que projet collectif et scientifique, _Stylo_ est coordonné par un groupe de chercheurs, de chercheuses, d'étudiants et d'étudiantes, en partenariat avec le diffuseur canadien Érudit, la très grande infrastructure de recherche française Huma-Num, la chaîne de publication Métopes à Caen, et des revues partenaires au Canada et en Europe.
Il ne s'agit pas tant de _financer_ les développements — majoritairement réalisés par des prestataires —, que de susciter un dialogue autour des pratiques d'écriture et d'édition, autant pour les auteurs et les autrices, les structures d'édition, les diffuseurs ainsi que les organismes de soutien à la recherche.

Avant de présenter et d'analyser le _module d'export_ de _Stylo_, nous devons préciser que cet éditeur de texte sémantique n'est pas le seul à se baser sur ce trio de formats et sur Pandoc.
Une communauté académique assez substantielle utilise ce mode d'écriture pour des productions académiques, nécessitant une littératie numérique relativement importante — utilisation d'un éditeur de texte, d'un terminal et d'un outil capable de générer le format BibTeX.
Le logiciel Zettlr a justement été développé comme une surcouche logicielle pour faciliter ces pratiques, proposant des fonctionnalités identiques ou très similaires à celles de _Stylo_, tout en se démarquant notamment sur le plan de la _solution_ logicielle — plutôt que comme un projet de recherche.
L'argumentaire présent sur le site web de Zettlr{{< n >}}[https://zettlr.com](https://zettlr.com){{< /n>}} adopte les codes des logiciels d'écriture disponibles sur le _marché_, malgré le fait qu'il soit d'abord un outil pensé par et pour la communauté scientifique, et qu'il soit un logiciel libre (sous licence GNU General Public License v3.0).
Zettlr met en avant des arguments pour se positionner face aux traitements de texte ou autres applications d'écriture, constituant ainsi une _alternative_ plus qu'un changement de paradigme profond.
Contrairement à _Stylo_, Zettlr n'a pas encore vocation à intégrer d'autres services développés par la communauté scientifique, pas plus que de se brancher à des flux de diffusion.
Il n'en demeure pas moins que Zettlr est une réussite logicielle, et une réponse qui était attendue et qui est désormais adoptée par une large communauté.
Un des éléments que Zettlr ne propose qu'en partie, c'est un ensemble de fonctionnalités d'export adaptées à la diffusion scientifique, que nous abordons désormais.


### 4.5.3. Les formats du module d'export

Une des spécificités de _Stylo_ est de proposer des modélisations éditoriales répondant aux exigences académiques et permettant ainsi un vaste choix de formats d'export.
Si, d'une certaine façon, cet éditeur de texte sémantique n'est qu'une application du format Markdown et du convertisseur Pandoc, cette modélisation constitue une originalité ainsi qu'une plus-value manifeste pour les personnes qui l'utilisent.
Ce que nous appelons _module d'export_ est une partie intégrante et néanmoins distincte de l'application d'écriture à proprement parler, comme nous pouvons le voir dans le schéma ci-dessous.

{{< figure type="figure" src="schema-export-stylo.png" legende="Schéma des différentes parties qui composent l'éditeur de texte sémantique _Stylo_ dans son ensemble" >}}

Nous décrivons ici les principes sur lesquels se fonde ce module d'export, ainsi que son fonctionnement (incluant l'usage de Pandoc) en mentionnant son historique ainsi que son développement.
Loin d'être un service _en plus_ de l'éditeur de texte, ce module d'export participe à la formalisation de l'acte d'édition sémantique possible avec _Stylo_.

Avant de lister les formats d'export possibles, il faut préciser que _Stylo_ stocke chacune des données dans une base de données, et expose ces informations via une API (_Application Programming Interface_ pour interface de programmation d’application en français) GraphQL.
Cette API permet d'accéder aux données sans passer par l'interface graphique de _Stylo_, et c'est ce que fait précisément le module d'export ; cette API est aussi conçue pour permettre l'accès aux données à d'autres applications, comme un CMS par exemple.
Chaque demande d'export déclenche un processus qui consiste à aller chercher les données dans les champs correspondants de cette base de données, pour ensuite les traiter.

Les formats d'export proposés reflètent les besoins divers en édition scientifique, et notamment la nécessité de disposer d'artefacts structurés, ou prenant en compte les contraintes liées à l'_édition sémantique_ désormais incontournables {{< cite "kembellec_semantic_2020" >}}.
Le premier format est le format HTML, utile d'abord pour obtenir un rendu graphique et sémantique dans le même environnement que _Stylo_, le Web, mais aussi pour une intégration manuelle dans des CMS.
Le format PDF offre une version paginée, avec un accès au format LaTeX pour des modifications avec le système de composition du même nom.
Les formats DOCX et ODT permettent un retour au traitement de texte, en sachant que les exports dans ces formats contiennent une feuille de style par défaut qui facilite l'édition alors en partie structurée dans ces environnements WYSIWYG.
Pour une intégration dans un logiciel de publication assistée par ordinateur (comme le logiciel InDesign), l'export au format ICML est proposé.
Pour une interopérabilité avec les diffuseurs numériques chargés de rendre disponibles les documents aux communautés scientifiques, plusieurs formats XML sont générés avec trois schémas : TEI (light), Érudit, et TEI Commons Publishing (partagé par Métopes et OpenEdition).
Enfin, les fichiers sources eux-mêmes (Markdown, YAML, BibTeX) peuvent être téléchargés.
Chaque format prend en compte une série de spécifications qui proviennent des besoins des revues et des diffuseurs, et qui se traduisent par une modélisation que nous détaillons désormais.


### 4.4.4. La modélisation dans le processus d'export

Construire une modélisation éditoriale consiste à définir un gabarit pour baliser convenablement les données (texte, informations sémantiques sur le texte, métadonnées, données bibliographiques, style bibliographique, et paramètres indiqués au moment de l'export) pour constituer un format qui répond à des standards précis.
Un gabarit est établi pour chaque format d'export.
Pandoc — présenté précédemment{{< renvoi chapitre="4" section="3" >}} — est chargé de convertir les fichiers sources selon un modèle, en fonction du format demandé, et selon des paramètres indiqués au moment de l'export — par exemple : faut-il afficher la table des matières ?
Pandoc propose son propre langage de _template_, voici un extrait du gabarit pour l'export au format HTML :

{{< code type="code" legende="Extrait du _template_ HTML du module d'export de _Stylo_, dans le langage de gabarit de Pandoc" >}}
<!--Indexation auteur de l'article-->
$if(authors)$
$for(authors)$
  <span property="author">$authors.forename$ $authors.surname$</span>
$endfor$
$endif$
{{< /code >}}

Dans les quelques lignes de code ci-dessus nous pouvons voir que Pandoc utilise un système de _variables_, ici `authors` et `forename` par exemple, qui correspondent aux clés des champs dans le fichier de métadonnées.
Pandoc a recours à des fonctions issues de la programmation, comme ici une condition avec `if` (si une condition est remplie alors la partie du _template_ qui suit est activée, jusqu'au `endif`), ou une boucle avec `for` (qui permet de récupérer une série de données et d'y appliquer une règle jusqu'à `endfor`).
Pour le dire autrement, si le document YAML contenant les métadonnées ne comporte aucune information en face de la clé `authors` alors la ligne qui suit n'est pas appliquée.
Si une donnée est renseignée en face de `authors`, alors Pandoc applique la suite du _template_ autant de fois qu'il y a de données dans les sous-champs `forename` et `surname`.
Si Pandoc propose des modèles par défaut, il est possible de définir entièrement un gabarit, ce qui est le cas pour _Stylo_.
Les exports XML nécessitent une étape supplémentaire qui consiste à appliquer une feuille de transformations XSL/XSLT sur un contenu structuré en XML ou en HTML.
Nous ne détaillons pas plus ces modèles, ils sont par ailleurs développés sous licence libre et disponible en ligne {{< cite "ecrinum_stylo_2023" >}}.

Pour appliquer ces différents modèles, Pandoc a été _apéifié_.
Ce néologisme indique qu'une couche supplémentaire permet de séparer strictement les différentes commandes nécessaires à Pandoc en créant une liste de paramètres que l'API peut prendre en compte.
Ainsi cette API traite une information en entrée, afin de composer en sortie les commandes qui sont ensuite appliquées avec Pandoc.
_Stylo_ est donc constitué de trois éléments : l'éditeur de texte (`stylo`), le module d'export (`stylo-export`), et la Pandoc API (`pandoc-api`).
L'éditeur de texte propose des options d'export aux utilisateurs et aux utilisatrices qui sélectionnent le format et les options souhaitées, ces informations sont transmises au module d'export qui construit les commandes nécessaires à la conversion, grâce à la Pandoc API, sur les sources via l'API GraphQL.
Une fois les commandes de conversion appliquées, les formats produits par le module d'export (toujours avec l'aide de la Pandoc API) sont transmis aux utilisateurs et utilisatrices via l'éditeur de texte — l'interface principale.
Cette décomposition en trois éléments distincts offre un fonctionnement structuré séparant ce qui dépend de l'écriture et de l'édition, et permettant d'adopter un développement dit _modulaire_ qui facilite les évolutions générales et spécifiques.

Cette description technique est essentielle pour comprendre les enjeux théoriques derrière ces programmes.
Distinguer les modèles des règles de conversion apporte une meilleure compréhension des mécanismes techniques auxquels les auteurs, les autrices, les éditeurs et les éditrices peuvent contribuer.
Avant d'aborder ce point nous devons expliciter l'usage et le rôle du programme Pandoc.


### 4.5.5. Pandoc : la raison d'une singularité hégémonique

Quel rôle joue Pandoc dans le développement de ce module d'export et donc dans la modélisation des contenus ?
Si Pandoc se définit lui-même comme un outil "universel", il convient d'interroger les contraintes qu'il impose et la raison d'une forme d'hégémonie de ce convertisseur de formats texte.

Nous ne détaillons pas à nouveau le fonctionnement de Pandoc, chose déjà faite dans la section précédente{{< renvoi chapitre="4" section="3" >}}, nous pouvons toutefois rappeler trois caractéristiques de ce logiciel.
Tout d'abord Pandoc _convertit_ des fichiers d'un format de balisage à un autre via un mécanisme complexe reposant sur un analyseur syntaxique, un arbre syntaxique abstrait et des modules d'écriture pour manipuler les données.
Pour simplifier, il applique des règles de conversion permettant de passer d'une expression sémantique balisée à une autre.
Ensuite Pandoc fonctionne en ligne de commandes, dans un terminal, ce qui signifie qu'il ne dispose pas d'interface graphique et qu'il peut être difficile d'accès pour certaines personnes.
Enfin Pandoc adopte un fonctionnement commun à d'autres programmes en ligne de commandes, consistant en des options et des arguments.
La commande `pandoc -f markdown -t html fichier-source.md -o fichier-converti.md` correspond donc à la conversion du fichier `fichier-source.md` en un fichier HTML `fichier-converti.html`.

Pandoc est devenu incontournable dans l'environnement des langages de balisage, la prise en charge de nombreux formats en entrée et en sortie en fait un "couteau suisse" de l'édition.
D'autres _parseurs_ existent, souvent limités à un format en entrée et un format en sortie, comme Markdown et HTML — comme nous l'avons vu précédemment avec l'initiative Babelmark{{< renvoi chapitre="4" section="3" >}} qui liste les multiples convertisseurs Markdown et leur saveur associée.
Les très nombreuses règles et conditions permettant le passage d'un format à un autre nécessitent des développements longs pour prendre en compte des cas d'usage parfois complexes.
Ceci explique pourquoi d'autres initiatives similaires n'ont pas vu le jour, tant les efforts investis dans Pandoc ont déjà été importants.
John MacFarlane note lui-même que l'engouement autour de sa création a été rapide, d'abord chez des personnes dans le domaine académique, puis pour plusieurs types d'applications faisant usage de langages de balisage.
Notons également que ce convertisseur, aussi utilisé soit-il, est créé, développé et maintenu par un professeur de philosophie — avec quelques personnes qui contribuent désormais également au projet.

L'universalité affichée sur le site web de Pandoc est-elle compatible avec son statut hégémonique ?
Un rapide coup d'œil au dépôt du code de Pandoc{{< n >}}https://github.com/jgm/pandoc{{< /n >}} permet de comprendre que les développements et les adaptations se font dans un souci d'interopérabilité, et non pour servir les intérêts d'une entreprise ou d'une personne en particulier.
Le développement de Pandoc est clairement tourné vers la communauté, prenant en compte les différents usages liés aux standards dans le domaine de la publication.
Toutefois, si le travail collaboratif est permis, le choix du langage de développement de Pandoc limite les contributions directes sur le code.
Pandoc est écrit en Haskell, un langage peu répandu avec un haut niveau d'abstraction.
Il est en effet basé sur le principe de programmation purement fonctionnelle, dont les opérations reposent uniquement sur l'évaluation de fonctions mathématiques — pour résumer ce principe grossièrement.
Ce choix s'explique par l'intérêt du philosophe pour les mathématiques et la logique, Pandoc a d'ailleurs d'abord été un bac à sable pour l'apprentissage de ce langage par John MacFarlane.
Le prix de l'implémentation d'un convertisseur d'une telle ampleur — en termes de nombre de langages de balisage pris en charge — est donc désormais une connaissance approfondie de Haskell, ainsi qu'une compréhension de la complexité de la structure actuelle du code.
Enfin, la priorité mise sur Markdown, HTML et LaTeX a des effets de bord sur d'autres formats d'export tels que XML-TEI.
À titre d'exemple, les données riches des bibliographies ne sont actuellement pas conservées dans l'export au format XML-TEI, comme indiqué dans un ticket{{< n >}}[https://github.com/jgm/pandoc/issues/8790](https://github.com/jgm/pandoc/issues/8790){{< /n >}} lié au développement du module de _Stylo_.
Il est nécessaire de préciser ces détails pour comprendre l'origine de cet outil de conversion ainsi que son développement continu depuis dix-sept ans, ce qui en fait par ailleurs un exemple de longévité dans le domaine du logiciel libre.

Pour terminer sur cette présentation technique du module d'export de _Stylo_, soulignons que tous les efforts se concentrent désormais sur une implémentation aussi agnostique que possible de Pandoc.
Cela signifie d'une part que le module `pandoc-api` est conçu comme une surcouche de Pandoc, des commandes entièrement compatibles avec le convertisseur sont ainsi élaborées, sans traitement supplémentaire ; d'autre part l'usage de _templates_ et de filtres facilite la séparation entre les possibilités du programme et la modélisation elle-même.
Ce choix de développement, réalisé par David Larlet avec la Chaire de recherche du Canada sur les écritures numériques, participe directement à cette modélisation éditoriale, plusieurs des implications épistémologiques induites sont explicitées par la suite.


### 4.5.6. Implications épistémologiques de la modélisation

Le module d'export de _Stylo_ est ainsi composé de plusieurs éléments, cette organisation reflète une modélisation éditoriale nécessaire pour les revues et les diffuseurs.

La première version du module d'export a été l'occasion d'un prototypage expérimental sous la forme d'un script Bash{{< n >}}Bash est un interpréteur en ligne de commande, il peut être écrit sous la forme d'un fichier, ce script peut ensuite être exécuté par une console.{{< /n >}} complexe, cette version démontre l'intérêt mais aussi les limites du bricolage dans le cas de _Stylo_.
Rappelons un moment de l'histoire de cet éditeur de texte sémantique : en 2018 lors de la mise en ligne du prototype développé par Marcello Vitali-Rosati, Servanne Monjour, Nicolas Sauret et Arthur Juchereau{{< n >}}Notons également les contributions d'Emmanuel Château-Dutier et de Michael Sinatra.{{< /n >}}, l'export n'est pas encore possible _en ligne_.
Pour pallier le fait que les utilisateurs et les utilisatrices devaient télécharger les sources puis appliquer localement des conversions avec Pandoc (auquel il fallait ajouter l'installation d'une distribution de LaTeX pour la génération des PDF), Marcello Vitali-Rosati écrit rapidement un script Bash{{< n >}}Voir en ligne : [https://framagit.org/stylo-editeur/process/-/blob/book/cgi-bin/exportArticle/exec.cgi](https://framagit.org/stylo-editeur/process/-/blob/book/cgi-bin/exportArticle/exec.cgi){{< /n >}} destiné à déléguer cette tâche à un serveur.
Ce _bricolage_ va pourtant rester la seule manière d'exporter des articles pendant plus de trois ans, avec de nombreuses améliorations apportées au script original.
L'usage d'une version précise de Pandoc, ainsi que la lourde installation de LaTeX, obligent à gérer ce module d'export dans un conteneur isolé avec le logiciel Docker.
Les contraintes du script Bash et le manque de structure des modèles (_templates_) ont conduit à une refonte complète de cette partie de _Stylo_ en 2021.

Le module d'export se devait d'être modularisé pour répondre aux exigences épistémologiques de départ, soit le fait de s'extraire d'une confusion entre structure et mise en forme, de transcrire sémantiquement le sens, et enfin de façonner nos propres outils d'écriture et d'édition en tant que communauté scientifique.
La phase de prototypage décrite ci-dessus a permis de modéliser ces fonctionnalités d'export avant de trouver et d'implémenter une solution plus cohérente et plus stable.
Cette étape de recherche a également accompagné des revues dans leur usage, l'écriture du code étant conjointe à celle des articles.
Insistons sur le fait que les développements de _Stylo_ sont continus, et que les phases de création de nouvelles fonctionnalités, d'amélioration ou de résolution de problèmes sont imbriquées dans les étapes d'accompagnement des revues, de formation ou de discussions régulières avec les partenaires.

_Stylo_ et son module d'export impliquent deux modularisations intrinsèquement liées : d'une part les conditions d'application des principes du _single source publishing_, et d'autre part la constitution d'une chaîne d'édition modulaire.
Il s'agit tout d'abord de distinguer les strates d'écriture dans _Stylo_ en quatre couches, nécessaires pour la réalisation d'une publication multimodale à partir d'une source unique : les sources (au nombre de trois) ; les modèles pour les différents types et formats d'export ; les feuilles de styles pour la mise en forme des artefacts, très fortement liées aux modèles ; les programmes, scripts et filtres pour la réalisation des exports.
Puisque nous avons déjà détaillé ces programmes ou scripts (éditeur de texte, module d'export et API Pandoc), nous pouvons préciser que Pandoc permet également d'utiliser des filtres pour ajouter des règles de conversion au moment du traitement des données.
Cette double modularisation est une volonté de disposer d'une double dimension interopérable : au niveau des contenus et au niveau des opérations de productions des artefacts.
Il s'agit de l'intégration d'une sémantique non plus seulement aux textes mais à l'édition elle-même.
Ces développements se traduisent néanmoins par des choix et des arbitrages qui relèvent parfois du compromis.

Cette modélisation est le résultat de la prise en compte de plusieurs paramètres : les contraintes de l'édition scientifique telles que présentées plus haut, des besoins propres aux revues académiques en fonction de leur domaine, et de certaines prérogatives des diffuseurs numériques.
Les revues ont des spécificités qui dépendent de leur champ, par exemple l'usage récurrent de figures, une mise en page particulière pour les versions numérique et imprimée, ou l'insertion d'extraits de code.
Pour que les articles des revues puissent être disponibles sur plusieurs plateformes académiques, les diffuseurs imposent des schémas XML.
Les besoins sont globalement similaires d'un diffuseur à un autre, mais leur formalisation diffère, et ainsi plusieurs schémas co-existent : JATS en Amérique du Nord, Érudit au Canada, et TEI Commons Publishing en Europe.
Trois exemples de schémas XML permettent la diffusion numérique d'articles scientifiques.
Le module d'export intègre le schéma d'Érudit depuis ses débuts, et l'implémentation du schéma TEI Commons Publishing a été mise en production en 2023.
Notons que pour permettre une automatisation complète afin de générer les formats pour une diffusion numérique — soit le fait de convertir les sources dans les formats XML idoines sans intervention sur les formats de sortie XML —, des ajustements sont nécessaires : dans la modélisation des données (textes et métadonnées) et dans les pratiques de balisages des personnes écrivant ou éditant avec _Stylo_.
Cela signifie qu'un accompagnement facilite l'utilisation de _Stylo_, l'éditeur de texte bénéficiant par là même de retours utiles à son amélioration.

Nous n'avons pas abordé la fonctionnalité d'export de livres, mais elle répond aux mêmes exigences académiques, et il s'agit d'une modélisation proche de celle exposée jusqu'ici.
Les articles constituent les chapitres ou les parties d'une monographie.
La description de cet objet éditorial est néanmoins déléguée dans un espace additionnel — sous la forme d'un fichier YAML supplémentaire —, afin de lever toute ambiguïté sur les niveaux de granularité (chapitres _vs_ livre).

Comment un format s'incarne-t-il dans des pratiques d'édition ?
Cette étude de cas, après les développements théoriques qui l'ont précédée, répond à cette question.
Les enjeux de la modélisation éditoriale, et plus spécifiquement dans le contexte de la publication scientifique, nous amènent à observer avec une attention accrue l'interconnexion entre des initiatives d'édition et la constitution de chaînes d'édition.
Ce que nous analysons ici dans le champ de l'édition académique, notamment en raison de sa position pionnière, est transposable à d'autres domaines des _lettres_.
À quel moment la fabrication de processus d'édition dépend-elle d'actes éditoriaux, et inversement ?
Quelles sont les conditions d'émergence des _fabriques_ d'édition ?
C'est l'objet du prochain et dernier chapitre.

